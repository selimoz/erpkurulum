﻿namespace KurulumArac
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txCommand = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabSonuc = new System.Windows.Forms.TabPage();
            this.GRD = new System.Windows.Forms.DataGridView();
            this.tabBilgi = new System.Windows.Forms.TabPage();
            this.txSonuc = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btTemizle = new System.Windows.Forms.Button();
            this.btCalistir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuTumSec = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabSonuc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GRD)).BeginInit();
            this.tabBilgi.SuspendLayout();
            this.panel1.SuspendLayout();
            this.cmenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // txCommand
            // 
            this.txCommand.Dock = System.Windows.Forms.DockStyle.Top;
            this.txCommand.Location = new System.Drawing.Point(0, 36);
            this.txCommand.Multiline = true;
            this.txCommand.Name = "txCommand";
            this.txCommand.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txCommand.Size = new System.Drawing.Size(668, 128);
            this.txCommand.TabIndex = 0;
            this.txCommand.TextChanged += new System.EventHandler(this.txCommand_TextChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabSonuc);
            this.tabControl1.Controls.Add(this.tabBilgi);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 164);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(668, 230);
            this.tabControl1.TabIndex = 1;
            // 
            // tabSonuc
            // 
            this.tabSonuc.Controls.Add(this.GRD);
            this.tabSonuc.Location = new System.Drawing.Point(4, 22);
            this.tabSonuc.Name = "tabSonuc";
            this.tabSonuc.Padding = new System.Windows.Forms.Padding(3);
            this.tabSonuc.Size = new System.Drawing.Size(660, 204);
            this.tabSonuc.TabIndex = 0;
            this.tabSonuc.Text = "İşlem Sonucu";
            this.tabSonuc.UseVisualStyleBackColor = true;
            // 
            // GRD
            // 
            this.GRD.AllowUserToAddRows = false;
            this.GRD.AllowUserToDeleteRows = false;
            this.GRD.AllowUserToOrderColumns = true;
            this.GRD.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.GRD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GRD.ContextMenuStrip = this.cmenu;
            this.GRD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GRD.Location = new System.Drawing.Point(3, 3);
            this.GRD.Name = "GRD";
            this.GRD.ReadOnly = true;
            this.GRD.Size = new System.Drawing.Size(654, 198);
            this.GRD.TabIndex = 0;
            this.GRD.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GRD_CellContentClick);
            // 
            // tabBilgi
            // 
            this.tabBilgi.Controls.Add(this.txSonuc);
            this.tabBilgi.Location = new System.Drawing.Point(4, 22);
            this.tabBilgi.Name = "tabBilgi";
            this.tabBilgi.Padding = new System.Windows.Forms.Padding(3);
            this.tabBilgi.Size = new System.Drawing.Size(660, 204);
            this.tabBilgi.TabIndex = 1;
            this.tabBilgi.Text = "Bilgi";
            this.tabBilgi.UseVisualStyleBackColor = true;
            // 
            // txSonuc
            // 
            this.txSonuc.BackColor = System.Drawing.Color.LightGray;
            this.txSonuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txSonuc.Location = new System.Drawing.Point(3, 3);
            this.txSonuc.Multiline = true;
            this.txSonuc.Name = "txSonuc";
            this.txSonuc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txSonuc.Size = new System.Drawing.Size(654, 198);
            this.txSonuc.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btTemizle);
            this.panel1.Controls.Add(this.btCalistir);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(668, 36);
            this.panel1.TabIndex = 2;
            // 
            // btTemizle
            // 
            this.btTemizle.Dock = System.Windows.Forms.DockStyle.Right;
            this.btTemizle.Location = new System.Drawing.Point(514, 4);
            this.btTemizle.Name = "btTemizle";
            this.btTemizle.Size = new System.Drawing.Size(75, 28);
            this.btTemizle.TabIndex = 5;
            this.btTemizle.Text = "Temizle";
            this.btTemizle.UseVisualStyleBackColor = true;
            // 
            // btCalistir
            // 
            this.btCalistir.Dock = System.Windows.Forms.DockStyle.Right;
            this.btCalistir.Location = new System.Drawing.Point(589, 4);
            this.btCalistir.Name = "btCalistir";
            this.btCalistir.Size = new System.Drawing.Size(75, 28);
            this.btCalistir.TabIndex = 4;
            this.btCalistir.Text = "Çalıştır";
            this.btCalistir.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 28);
            this.label1.TabIndex = 3;
            this.label1.Text = "Sorgu Çalıştır";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmenu
            // 
            this.cmenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuTumSec});
            this.cmenu.Name = "cmenu";
            this.cmenu.Size = new System.Drawing.Size(142, 26);
            // 
            // menuTumSec
            // 
            this.menuTumSec.Name = "menuTumSec";
            this.menuTumSec.Size = new System.Drawing.Size(141, 22);
            this.menuTumSec.Text = "Tümünü Seç";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 394);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txCommand);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabSonuc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GRD)).EndInit();
            this.tabBilgi.ResumeLayout(false);
            this.tabBilgi.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.cmenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txCommand;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabSonuc;
        private System.Windows.Forms.TabPage tabBilgi;
        private System.Windows.Forms.TextBox txSonuc;
        private System.Windows.Forms.DataGridView GRD;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btTemizle;
        private System.Windows.Forms.Button btCalistir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip cmenu;
        private System.Windows.Forms.ToolStripMenuItem menuTumSec;
    }
}