﻿namespace KurulumArac
{
    partial class NYeniData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NYeniData));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDataAd = new System.Windows.Forms.TextBox();
            this.btnYeniData = new System.Windows.Forms.Button();
            this.lbDurum = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkBlue;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(453, 105);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.AliceBlue;
            this.label2.Location = new System.Drawing.Point(12, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(363, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "ERP 12 için boş bir veritabanı oluşturur. Oluşturmak istediğiniz veritabanı adını" +
    " yazıp Oluştur\'u tıklayınız.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Yeni Data Oluştur";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Yeni Veritabanı Adı";
            // 
            // txtDataAd
            // 
            this.txtDataAd.Location = new System.Drawing.Point(128, 112);
            this.txtDataAd.Name = "txtDataAd";
            this.txtDataAd.Size = new System.Drawing.Size(245, 22);
            this.txtDataAd.TabIndex = 2;
            // 
            // btnYeniData
            // 
            this.btnYeniData.Location = new System.Drawing.Point(379, 111);
            this.btnYeniData.Name = "btnYeniData";
            this.btnYeniData.Size = new System.Drawing.Size(62, 23);
            this.btnYeniData.TabIndex = 3;
            this.btnYeniData.Text = "Oluştur";
            this.btnYeniData.UseVisualStyleBackColor = true;
            this.btnYeniData.Click += new System.EventHandler(this.btnYeniData_Click);
            // 
            // lbDurum
            // 
            this.lbDurum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbDurum.Location = new System.Drawing.Point(41, 137);
            this.lbDurum.Name = "lbDurum";
            this.lbDurum.Size = new System.Drawing.Size(372, 107);
            this.lbDurum.TabIndex = 4;
            this.lbDurum.Text = "Durum";
            // 
            // NYeniData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(453, 253);
            this.Controls.Add(this.lbDurum);
            this.Controls.Add(this.btnYeniData);
            this.Controls.Add(this.txtDataAd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NYeniData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "NYeniData";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDataAd;
        private System.Windows.Forms.Button btnYeniData;
        private System.Windows.Forms.Label lbDurum;
    }
}