﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace KurulumArac
{
    public partial class NYeniData : Form
    {
        
        public NYeniData()
        {
            InitializeComponent();
        }

        private void btnYeniData_Click(object sender, EventArgs e)
        {
            if (txtDataAd.Text.Trim() == "")
            {
                lbDurum.Text = "Yeni Veritabanı Adı Giriniz\r\n";
                return;
            }
            String YeniData = txtDataAd.Text.Trim().ToUpper();
            lbDurum.Text = "Oluşturuluyor\r\n";
            if (File.Exists(Application.StartupPath + "\\data\\bos\\ERP12.mdf") && File.Exists(Application.StartupPath + "\\data\\bos\\ERP12_0.ldf"))
            {
                if (!Directory.Exists(Application.StartupPath + "\\Data\\" + YeniData))
                {
                    Directory.CreateDirectory(Application.StartupPath + "\\Data\\" + YeniData);
                    lbDurum.Text += "Klasör Oluşturuldu \r\n";
                }
                try
                {
                    File.Copy(Application.StartupPath + "\\data\\bos\\ERP12.mdf", Application.StartupPath + "\\Data\\" + YeniData + "\\ERP12.mdf");
                    File.Copy(Application.StartupPath + "\\data\\bos\\ERP12_0.ldf", Application.StartupPath + "\\Data\\" + YeniData + "\\ERP12_0.ldf");
                    lbDurum.Text += "Model Dosyalar Koplayandı \r\n";
                }
                catch (Exception ED)
                {
                    lbDurum.Text += "Model Dosyalar Kopyalamadı\r\n";
                    lbDurum.Text += ED.Message.ToString();
                    return;
                }
                /*
                 
                CREATE DATABASE [TEWST] ON 
( FILENAME = N'D:\ANKARA\ERP\KurulumArac\KurulumArac\bin\Debug\data\bos\ERP12.mdf' ),
( FILENAME = N'D:\ANKARA\ERP\KurulumArac\KurulumArac\bin\Debug\data\bos\ERP12_0.ldf' )
 FOR ATTACH
                 
                 */

                String Komut = "CREATE DATABASE [" + YeniData + "] ON ";
                Komut += @" ( FILENAME = N'"+ Application.StartupPath + "\\Data\\" + YeniData + "\\ERP12.mdf" + "' ),";
                Komut += @" ( FILENAME = N'"+ Application.StartupPath + "\\Data\\" + YeniData + "\\ERP12_0.ldf" + "' )";
                Komut += " FOR ATTACH";
                DataLayer.AktifDB.DataCatalog = "master";
                DataLayer.AktifDB.ConnectionStringCreate2();
                if (DataLayer.SQLCalistir(Komut, DataLayer.AktifDB))
                {
                    lbDurum.Text += YeniData + " Veritabanı Eklendi...!";
                    DataLayer.YeniDataAdi = YeniData;
                    this.Close();
                }
                else
                {
                    lbDurum.Text += "Veritabanı Eklenemedi...!";
                    lbDurum.Text += DataLayer.HataMesaj.Message.ToString() ;
                }

            }
            else
            {
                lbDurum.Text += "Model Dosyalar bulunamadı. Kurulum Tipini Server Seçmelisiniz.\r\n";
                lbDurum.Text += "Bulunamayan Dosyalar\r\n";
                lbDurum.Text += Application.StartupPath + "\\data\\bos\\ERP12.mdf\r\n";
                lbDurum.Text += Application.StartupPath + "\\data\\bos\\ERP12_0.ldf\r\n";

            }
        }
    }
}
