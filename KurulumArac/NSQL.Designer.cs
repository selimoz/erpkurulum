﻿namespace KurulumArac
{
    partial class NSQL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NSQL));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsAciklama = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txPassword = new System.Windows.Forms.TextBox();
            this.txInstansName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btKurBaslat = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmYeniParola = new System.Windows.Forms.ToolStripMenuItem();
            this.cmParolaGozukmesin = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(299, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsAciklama});
            this.statusStrip1.Location = new System.Drawing.Point(0, 233);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(299, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsAciklama
            // 
            this.tsAciklama.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsAciklama.Name = "tsAciklama";
            this.tsAciklama.Size = new System.Drawing.Size(284, 17);
            this.tsAciklama.Spring = true;
            this.tsAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsAciklama.Click += new System.EventHandler(this.tsAciklama_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txPassword);
            this.groupBox1.Controls.Add(this.txInstansName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 106);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(275, 77);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SQL Kurulum Ayarları";
            // 
            // txPassword
            // 
            this.txPassword.ContextMenuStrip = this.contextMenuStrip1;
            this.txPassword.Location = new System.Drawing.Point(103, 46);
            this.txPassword.Name = "txPassword";
            this.txPassword.Size = new System.Drawing.Size(166, 21);
            this.txPassword.TabIndex = 5;
            this.txPassword.Text = "Erp3633.";
            // 
            // txInstansName
            // 
            this.txInstansName.Location = new System.Drawing.Point(103, 19);
            this.txInstansName.Name = "txInstansName";
            this.txInstansName.Size = new System.Drawing.Size(166, 21);
            this.txInstansName.TabIndex = 3;
            this.txInstansName.Text = "SQL2014";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Sa Pasword";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            this.label4.DoubleClick += new System.EventHandler(this.label4_DoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Instans Name";
            // 
            // btKurBaslat
            // 
            this.btKurBaslat.Location = new System.Drawing.Point(12, 189);
            this.btKurBaslat.Name = "btKurBaslat";
            this.btKurBaslat.Size = new System.Drawing.Size(275, 36);
            this.btKurBaslat.TabIndex = 4;
            this.btKurBaslat.Text = "SQL Server Kurulumunu Başlat";
            this.btKurBaslat.UseVisualStyleBackColor = true;
            this.btKurBaslat.Click += new System.EventHandler(this.btKurBaslat_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmYeniParola,
            this.cmParolaGozukmesin});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(175, 48);
            // 
            // cmYeniParola
            // 
            this.cmYeniParola.Name = "cmYeniParola";
            this.cmYeniParola.Size = new System.Drawing.Size(180, 22);
            this.cmYeniParola.Text = "Yeni Parola Üret";
            this.cmYeniParola.Click += new System.EventHandler(this.cmYeniParola_Click);
            // 
            // cmParolaGozukmesin
            // 
            this.cmParolaGozukmesin.CheckOnClick = true;
            this.cmParolaGozukmesin.Name = "cmParolaGozukmesin";
            this.cmParolaGozukmesin.Size = new System.Drawing.Size(180, 22);
            this.cmParolaGozukmesin.Text = "Parola Görükmesin";
            this.cmParolaGozukmesin.CheckedChanged += new System.EventHandler(this.cmParolaGozukmesin_CheckedChanged);
            this.cmParolaGozukmesin.Click += new System.EventHandler(this.cmParolaGozukmesin_Click);
            // 
            // NSQL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 255);
            this.Controls.Add(this.btKurBaslat);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NSQL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SQL Server 2014 Kurulum";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txPassword;
        private System.Windows.Forms.TextBox txInstansName;
        private System.Windows.Forms.ToolStripStatusLabel tsAciklama;
        private System.Windows.Forms.Button btKurBaslat;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cmYeniParola;
        private System.Windows.Forms.ToolStripMenuItem cmParolaGozukmesin;
    }
}