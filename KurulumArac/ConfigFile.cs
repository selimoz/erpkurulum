﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
namespace KurulumArac
{
    class ConfigFile
    {
        private String dosyaAdi;
        private XmlDocument Icerik;
        XmlNode Nconfiguration;
        private XmlNode NconnectionStrings;
        public string DosyaAdi { get => dosyaAdi; set => dosyaAdi = value; }
        public ConfigFile()
        {
            Icerik = new XmlDocument();
            XmlDeclaration XmlDec = Icerik.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlElement root = Icerik.DocumentElement;
            Icerik.InsertBefore(XmlDec, root);

            Nconfiguration = Icerik.CreateElement("configuration");
            XmlNode NconfigSections = Icerik.CreateElement("configSections");
            NconnectionStrings = Icerik.CreateElement("connectionStrings");



            ////<? xml version = "1.0" encoding = "utf-8" ?>

            //XmlNode MainNod = Icerik.CreateElement("? xml");
            //XmlAttribute aVersiyon = Icerik.CreateAttribute("versiyon");
            //XmlAttribute aEncoding = Icerik.CreateAttribute("encoding");

            //aVersiyon.Value = "1.0";
            //aEncoding.Value = "utf-8";

            //MainNod.Attributes.Append(aVersiyon);
            //MainNod.Attributes.Append(aEncoding);

            Icerik.AppendChild(Nconfiguration);
            Nconfiguration.AppendChild(NconfigSections);
            Nconfiguration.AppendChild(NconnectionStrings);

        }
        public void ConnectionStringAdd(String Name, String ConnectionString)
        {
            XmlNode Ncs = Icerik.CreateElement("add");
            XmlAttribute aname = Icerik.CreateAttribute("name");
            XmlAttribute aconnectionString = Icerik.CreateAttribute("connectionString");
            XmlAttribute aproviderName = Icerik.CreateAttribute("providerName");
            aname.Value = Name;
            aconnectionString.Value = ConnectionString;
            aproviderName.Value = "System.Data.SqlClient";

            Ncs.Attributes.Append(aname);
            Ncs.Attributes.Append(aconnectionString);
            Ncs.Attributes.Append(aproviderName);

            NconnectionStrings.AppendChild(Ncs);

        }
        public XmlNode NodEkle(String name, String[] Attribs)
        {
            XmlNode XN = Icerik.CreateElement(name);
            if (Attribs.Length > 0)
            {
                foreach (String Attr in Attribs)
                {
                    if (Attr != "")
                    {
                        String[] Alanlar = Attr.Split('=');
                        XmlAttribute IA = Icerik.CreateAttribute(Alanlar[0].ToString());
                        if (Alanlar.Length > 1) IA.Value = Attr.Substring(Attr.IndexOf("=", 0) + 1, Attr.Length - Attr.IndexOf("=", 0) - 1); else IA.Value = "";
                        XN.Attributes.Append(IA);
                    }
                }
            }
            return XN;
        }
        public XmlNode NodEkleAciklama(String Metin)
        {
            XmlNode XN = Icerik.CreateComment(Metin);
            return XN;
        }
        public void DigerIcerikler()
        {

            // APP CONFIG

            XmlNode NappSettings = Icerik.CreateElement("appSettings");
            String[] K1 = { "key=Language", "value=tr-TR" };
            String[] K2 = { "key=IN-MEMORY", "value=EVETT" };
            String[] K3 = { "key=WECART-appKey", "value=test" };
            String[] K4 = { "key=WECART-appSecret", "value=test" };
            String[] K5 = { "key=ClientSettingsProvider.ServiceUri", "" };



            NappSettings.AppendChild(NodEkle("add", K1));
            NappSettings.AppendChild(NodEkle("add", K2));
            NappSettings.AppendChild(NodEkle("add", K3));
            NappSettings.AppendChild(NodEkle("add", K4));
            NappSettings.AppendChild(NodEkle("add", K5));

            NappSettings.AppendChild(NodEkleAciklama("<add key=\"Language\" value=\"en-EN\"/>"));
            NappSettings.AppendChild(NodEkleAciklama("<add key=\"Language\" value=\"ar-SA\"/>"));
            NappSettings.AppendChild(NodEkleAciklama("<add key=\"Language\" value=\"ru-RU\"/>"));


            Nconfiguration.AppendChild(NappSettings);

            // system.diagnostics

            XmlNode Nsystemdiagnostics = Icerik.CreateElement("system.diagnostics");
            XmlNode Nsources = Icerik.CreateElement("sources");
            Nsources.AppendChild(NodEkleAciklama("This section defines the logging configuration for My.Application.Log"));
            XmlNode Nsource = Icerik.CreateElement("source");
            XmlAttribute At100 = Icerik.CreateAttribute("name"); At100.Value = "DefaultSource";
            XmlAttribute At101 = Icerik.CreateAttribute("switchName"); At101.Value = "DefaultSwitch";
            Nsources.AppendChild(Nsource);
            XmlNode Nlisteners = Icerik.CreateElement("listeners");
            String[] K10 = { "name=FileLog" };
            Nlisteners.AppendChild(NodEkle("add", K10));
            Nlisteners.AppendChild(NodEkleAciklama("Uncomment the below section to write to the Application Event Log"));
            Nlisteners.AppendChild(NodEkleAciklama("<add name=\"EventLog\"/>"));
            Nsource.AppendChild(Nlisteners);
            Nsystemdiagnostics.AppendChild(Nsources);

            XmlNode nswitches = Icerik.CreateElement("switches");
            String[] K11 = { "name=DefaultSwitch", "value=Information" };
            nswitches.AppendChild(NodEkle("add", K11));
            Nsystemdiagnostics.AppendChild(nswitches);

            XmlNode nsharedListeners = Icerik.CreateElement("sharedListeners");

            String[] K12 = { "name=FileLog", "type=Microsoft.VisualBasic.Logging.FileLogTraceListener, Microsoft.VisualBasic, Version = 8.0.0.0, Culture = neutral, PublicKeyToken = b03f5f7f11d50a3a, processorArchitecture = MSIL", "initializeData=FileLogWriter" };
            nsharedListeners.AppendChild(NodEkle("add", K12));
            nsharedListeners.AppendChild(NodEkleAciklama("Uncomment the below section and replace APPLICATION_NAME with the name of your application to write to the Application Event Log"));
            nsharedListeners.AppendChild(NodEkleAciklama("<add name=\"EventLog\" type=\"System.Diagnostics.EventLogTraceListener\" initializeData=\"APPLICATION_NAME\"/>"));
            Nsystemdiagnostics.AppendChild(nsharedListeners);

            Nconfiguration.AppendChild(Nsystemdiagnostics);

            // system.web

            XmlNode nsysweb = Icerik.CreateElement("sys.web");
            XmlNode nmembership = Icerik.CreateElement("membership");
            XmlAttribute At200 = Icerik.CreateAttribute("defaultProvider"); At200.Value = "ClientAuthenticationMembershipProvider";
            nmembership.Attributes.Append(At200);
            nsysweb.AppendChild(nmembership);
            XmlNode nproviders = Icerik.CreateElement("providers");
            /*
             name="ClientAuthenticationMembershipProvider" type="System.Web.ClientServices.Providers.ClientFormsAuthenticationMembershipProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri=""
             */
            String[] K13 = { "name=ClientAuthenticationMembershipProvider", "type=System.Web.ClientServices.Providers.ClientFormsAuthenticationMembershipProvider, System.Web.Extensions, Version = 4.0.0.0, Culture = neutral, PublicKeyToken = 31bf3856ad364e35", "serviceUri" };
            nproviders.AppendChild(NodEkle("add", K13));
            nmembership.AppendChild(nproviders);
            //<membership defaultProvider="ClientAuthenticationMembershipProvider">

            Nconfiguration.AppendChild(nsysweb);

        }
        public void Save()
        {
            DigerIcerikler();
            Icerik.Save("erp12.exe.config");
        }

    }
}
