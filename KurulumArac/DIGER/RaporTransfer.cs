﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KurulumArac.DIGER
{
    class RaporTransfer
    {
        public void RaporExport(Int32 raporId, String connetionString, String exportFile = "")
        {
            DataSet DS = new DataSet();
            DataTable tbRaporListe = GetTable("SELECT * FROM RAPOR_LISTE WHERE ID=" + raporId.ToString(), connetionString, "RAPOR_LISTE");
            DataTable tbRaporBelge = GetTable("SELECT * FROM RAPOR_BELGE WHERE RAPOR_ID=" + raporId.ToString(), connetionString, "RAPOR_BELGE");
            DataTable tbRaporAlan = GetTable("SELECT * FROM RAPOR_ALAN WHERE RAPOR_LISTE=" + raporId.ToString(), connetionString, "RAPOR_ALAN");
            DataTable tbRaporGorunum = GetTable("SELECT * FROM RAPOR_GORUNUM WHERE RAPOR_LISTE=" + raporId.ToString(), connetionString, "RAPOR_GORUNUM");


            if (tbRaporListe != null && tbRaporListe.Rows.Count > 0)
            {
                DataRow raporRow = tbRaporListe.Rows[0];
                String raporName = raporRow["RAPOR_ADI"].ToString();
                if (raporRow["SQLKODU"].ToString().Trim().ToUpper(new System.Globalization.CultureInfo("en-EN")).StartsWith("EXEC"))
                {

                    String raporProcedureName = String.Empty;
                    String[] raporSqlCodeStr = raporRow["SQLKODU"].ToString().Trim().Split(' ');
                    if (raporSqlCodeStr.Length > 1) raporProcedureName = raporSqlCodeStr[1];
                    if (!raporProcedureName.Equals(String.Empty))
                    {
                        DataTable tbProcedure = GetTable("select m.object_id, m.definition,p.name  from sys.all_sql_modules m inner join sys.procedures p on p.object_id=m.object_id where p.name='" + raporProcedureName + "'", connetionString, "PROCEDURE");
                        if (tbProcedure != null && tbProcedure.Rows.Count > 0) DS.Tables.Add(tbProcedure);
                    }
                }
                DS.Tables.Add(tbRaporListe);
                if (tbRaporAlan != null && tbRaporAlan.Rows.Count > 0) DS.Tables.Add(tbRaporAlan);
                if (tbRaporBelge != null && tbRaporBelge.Rows.Count > 0) DS.Tables.Add(tbRaporBelge);
                if (tbRaporGorunum != null && tbRaporGorunum.Rows.Count > 0) DS.Tables.Add(tbRaporGorunum);
                if (exportFile.Trim() == "") exportFile = raporName.Replace("...!", "").Replace(" ", "") + ".erpRapor";
                DS.WriteXml(exportFile, XmlWriteMode.WriteSchema);

            }


        }
        public void RaporImport(String raporFile, String connectionString, Boolean raporRefresh = false)
        {
            if (File.Exists(raporFile))
            {
                try
                {
                    DataSet DS = new DataSet();
                    DS.ReadXml(raporFile, XmlReadMode.ReadSchema);

                    if (DS.Tables.Contains("RAPOR_LISTE") && DS.Tables["RAPOR_LISTE"].Rows.Count > 0)
                    {

                        DataRow RW = DS.Tables["RAPOR_LISTE"].Rows[0];
                        /* Rapor Yedeklendi */
                        RaporExport(Int32.Parse(RW["ID"].ToString()), connectionString);
                        /* Procedure Oluştu */
                        if (DS.Tables.Contains("PROCEDURE") && DS.Tables["PROCEDURE"].Rows.Count > 0)
                        {
                            DataRow PRW = DS.Tables["PROCEDURE"].Rows[0];
                            String Code = PRW["definition"].ToString();
                            SqlExecute("DROP PROCEDURE " + PRW["name"].ToString(), connectionString);
                            SqlExecute(Code, connectionString);
                        }

                        /* Rapor Silindi */

                        if (raporRefresh) SqlExecute(@"EXEC RP_RAPORSIL " + RW["ID"].ToString(), connectionString);
                        DataSet RDS = newDataSet(DS, RW, connectionString);
                        SaveDataSet(RDS, connectionString);
                        MessageBox.Show("Rapor Kaydedildi.");
                    }
                }
                catch (Exception Exc)
                {
                    MessageBox.Show(Exc.Message.ToString());
                }

            }
        }
        private DataSet newDataSet(DataSet DS, DataRow RW, String connectionString)
        {
            DataSet RDS = new DataSet();
            DataTable tbRaporListe = GetTable("SELECT * FROM RAPOR_LISTE WHERE 1=0", connectionString, "RAPOR_LISTE");
            RDS.Tables.Add(tbRaporListe);
            RaporListeCreate(RW, tbRaporListe);


            if (DS.Tables.Contains("RAPOR_ALAN") && DS.Tables["RAPOR_ALAN"].Rows.Count > 0) RaporTabloCreate(RDS, DS.Tables["RAPOR_ALAN"], connectionString);
            if (DS.Tables.Contains("RAPOR_BELGE") && DS.Tables["RAPOR_BELGE"].Rows.Count > 0) RaporTabloCreate(RDS, DS.Tables["RAPOR_BELGE"], connectionString);
            if (DS.Tables.Contains("RAPOR_GORUNUM") && DS.Tables["RAPOR_GORUNUM"].Rows.Count > 0) RaporTabloCreate(RDS, DS.Tables["RAPOR_GORUNUM"], connectionString);

            return RDS;

        }
        private void RaporTabloCreate(DataSet RSD, DataTable TBL, String connectionString)
        {
            if (TBL.Rows.Count > 0)
            {
                DataTable tbl = GetTable("SELECT * FROM " + TBL.TableName + " WHERE 1=0", connectionString, TBL.TableName);
                foreach (DataRow RW in TBL.Rows)
                {
                    DataRow newRW = tbl.NewRow();
                    foreach (DataColumn col in TBL.Columns)
                    {
                        newRW[col.ColumnName] = RW[col.ColumnName];
                    }
                    tbl.Rows.Add(newRW);
                }
                if (tbl.Rows.Count > 0) RSD.Tables.Add(tbl);
            }
        }
        private void RaporListeCreate(DataRow RW, DataTable tbRaporListe)
        {
            DataRow yeniRaporRow = tbRaporListe.NewRow();
            foreach (DataColumn col in RW.Table.Columns)
            {
                yeniRaporRow[col.ColumnName] = RW[col.ColumnName];
            }
            tbRaporListe.Rows.Add(yeniRaporRow);
        }
        public DataTable GetTable(String komut, String connectionString, String tableName = "")
        {
            DataTable newtable = new DataTable();
            if (tableName != "") newtable.TableName = tableName;
            using (SqlConnection sCon = new SqlConnection(connectionString))
            {
                try
                {
                    sCon.Open();
                    using (SqlCommand sCmd = new SqlCommand(komut, sCon))
                    {
                        using (SqlDataAdapter da = new SqlDataAdapter(sCmd))
                        {
                            da.Fill(newtable);
                            return newtable;
                        }
                    }
                }
                catch (Exception)
                {
                    return newtable;
                }
            }
        }
        public Boolean SqlExecute(String komut, String connectionString)
        {
            using (SqlConnection sCon = new SqlConnection(connectionString))
            {
                try
                {
                    sCon.Open();
                    using (SqlCommand sCmd = new SqlCommand(komut, sCon))
                    {
                        sCmd.ExecuteNonQuery();
                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        public void SaveDataSet(DataSet DS, String connectionString)
        {
            foreach (DataTable TBL in DS.Tables)
            {
                using (SqlConnection sCon = new SqlConnection(connectionString))
                {
                    using (SqlDataAdapter sDa = new SqlDataAdapter("SELECT * FROM " + TBL.TableName + " Where 1=0", sCon))
                    {
                        SqlCommandBuilder sCmb = new SqlCommandBuilder(sDa);
                        sDa.InsertCommand = sCmb.GetInsertCommand();
                        sDa.UpdateCommand = sCmb.GetUpdateCommand();
                        sDa.Update(TBL);
                    }
                }
            }
        }
    }
}
