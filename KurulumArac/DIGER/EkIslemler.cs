﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace KurulumArac.DIGER
{
    class EkIslemler
    {
        public DatabaseInfo DBI;
        public EkIslemler(DatabaseInfo dbi)
        {
            DBI = dbi;
        }
        public void raporExportPanel(ref SplitContainer SContainer)
        {
            DataTable tbRaporList = new DataTable();
            DataLayer.TabloDoldur(ref tbRaporList, "SELECT R.* FROM FIRMASABITLERI F LEFT JOIN RAPOR_LISTE R ON 1=0 UNION ALL SELECT * FROM RAPOR_LISTE SELECT * FROM RAPOR_LISTE ORDER BY RAPOR_ADI", DBI);
            SContainer.Tag = DBI;
            RaporExportObjeCreate(SContainer, tbRaporList);
            return;
        }

        private static void RaporExportObjeCreate(SplitContainer SContainer, DataTable TBL)
        {
            Label lbRapor = new Label();
            ComboBox cmbRaporList = new ComboBox();
            Button btExport = new Button();
            Label lbBilgi = new Label();

            lbRapor.AutoSize = true;
            lbRapor.Location = new System.Drawing.Point(14, 16);
            lbRapor.Name = "lbRapor";
            lbRapor.Size = new System.Drawing.Size(54, 13);
            lbRapor.TabIndex = 0;
            lbRapor.Text = "Rapor Bul";
            // 
            // txRapoAra
            // 
            cmbRaporList.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbRaporList.FormattingEnabled = true;
            cmbRaporList.Location = new System.Drawing.Point(74, 13);
            cmbRaporList.Name = "cmbRaporList";
            cmbRaporList.Size = new System.Drawing.Size(258, 21);
            cmbRaporList.TabIndex = 4;
            // 
            // btExport
            // 
            btExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            btExport.Location = new System.Drawing.Point(338, 12);
            btExport.Name = "btExport";
            btExport.Size = new System.Drawing.Size(96, 22);
            btExport.TabIndex = 2;
            btExport.Text = "Rapor Export";
            btExport.UseVisualStyleBackColor = true;
            // 
            // lbBilgi
            // 
            lbBilgi.AutoSize = true;
            lbBilgi.ForeColor = System.Drawing.Color.Gray;
            lbBilgi.Location = new System.Drawing.Point(71, 36);
            lbBilgi.Name = "lbBilgi";
            lbBilgi.Size = new System.Drawing.Size(65, 13);
            lbBilgi.TabIndex = 3;
            lbBilgi.Text = "Rapor Bilgisi";

            SContainer.Panel2.Controls.Add(lbBilgi);
            SContainer.Panel2.Controls.Add(cmbRaporList);
            SContainer.Panel2.Controls.Add(lbRapor);
            SContainer.Panel2.Controls.Add(btExport);

            cmbRaporList.DataSource = TBL;
            cmbRaporList.DisplayMember = "RAPOR_ADI";
            cmbRaporList.ValueMember = "ID";

            cmbRaporList.SelectedIndexChanged += new EventHandler(raporSecildi);
            btExport.Click += new EventHandler(btExportEt);
        }
        private static void btExportEt(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            DatabaseInfo dbi2 = (DatabaseInfo)btn.Parent.Parent.Tag;
            if (btn.Parent.Tag != null)
            {
                DataRow Row = (DataRow)btn.Parent.Tag;
                Int32 raporId; if (!Int32.TryParse(Row["ID"].ToString(), out raporId)) raporId = 0;
                String raporAd = Row["RAPOR_ADI"].ToString().Replace(">", "").Replace("!", "").Replace("=", "").Replace("&", "").Replace("+", "").Replace(@"\", "").Replace("/", "").Replace(".", "").Replace(",", "");
                if (raporId > 0)
                {
                    String exportFileName = "";
                    using (SaveFileDialog fd = new SaveFileDialog())
                    {
                        fd.Title = "ERP12 Rapor Export";
                        fd.Filter = "ERP12 Rapor Dosyası|*.erpRapor";

                        fd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        fd.FileName = raporAd.Replace("..!", "").Replace(" ", "");
                        if (fd.ShowDialog() == DialogResult.OK)
                        {
                            exportFileName = fd.FileName;
                            RaporTransfer TRN = new RaporTransfer();
                            TRN.RaporExport(raporId, dbi2.ConnectionString, exportFileName);
                        }
                    }

                }
            }

        }
        private static void raporSecildi(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            Label lbl = (Label)nesneBul(cmb.Parent, "lbBilgi");
            if (cmb.SelectedIndex > -1)
            {
                DataTable tbl = (DataTable)cmb.DataSource;
                DataRow[] row = tbl.Select("ID=" + cmb.SelectedValue.ToString());
                if (row.Length > 0)
                {
                    cmb.Parent.Tag = row[0];

                    if (lbl != null) lbl.Text = row[0]["ACIKLAMA"].ToString();
                }
                else
                {
                    cmb.Parent.Tag = null;
                    if (lbl != null) lbl.Text = "Rapor Seçiniz";
                }
            }
        }
        private static Control nesneBul(Control Parent, String Name)
        {
            foreach (Control item in Parent.Controls)
            {
                if (item.Name == Name) return item;
            }
            return null;
        }

        public void fiyatDegisimExportPanel(ref SplitContainer SContainer)
        {
            SContainer.Tag = DBI;
            DataTable tbFiyatAd = new DataTable();
            DataLayer.TabloDoldur(ref tbFiyatAd, "SELECT * FROM STOK_FIYAT_AD", DBI);
            FiyatDegisimObjeCreate(SContainer, tbFiyatAd);
        }
        public void SorguPanel(ref SplitContainer SContainer)
        {
            SContainer.Tag = DBI;
            SorguCalistirObjeCreate(SContainer);
        }

        private void SorguCalistirObjeCreate(SplitContainer sContainer)
        {
            TextBox txCommand = new TextBox();
            TabControl tabControl1 = new TabControl();
            TabPage tabSonuc = new TabPage();
            TabPage tabBilgi = new TabPage();
            TextBox txSonuc = new TextBox();
            DataGridView GRD = new DataGridView();
            Panel panel1 = new Panel();
            Button btTemizle = new Button();
            Button btCalistir = new Button();
            Label label1 = new Label();

            sContainer.Panel2.Controls.Add(tabControl1);
            sContainer.Panel2.Controls.Add(txCommand);
            sContainer.Panel2.Controls.Add(panel1);
            // 
            // txCommand
            // 
            txCommand.Dock = DockStyle.Top;
            txCommand.Location = new System.Drawing.Point(0, 36);
            txCommand.Multiline = true;
            txCommand.Name = "txCommand";
            txCommand.ScrollBars = ScrollBars.Both;
            txCommand.Size = new System.Drawing.Size(668, 128);
            txCommand.TabIndex = 0;
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(tabSonuc);
            tabControl1.Controls.Add(tabBilgi);
            tabControl1.Dock = DockStyle.Fill;
            tabControl1.Location = new System.Drawing.Point(0, 164);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new System.Drawing.Size(668, 230);
            tabControl1.TabIndex = 1;
            // 
            // tabSonuc
            // 
            tabSonuc.Controls.Add(GRD);
            tabSonuc.Location = new System.Drawing.Point(4, 22);
            tabSonuc.Name = "tabSonuc";
            tabSonuc.Padding = new Padding(3);
            tabSonuc.Size = new System.Drawing.Size(660, 204);
            tabSonuc.TabIndex = 0;
            tabSonuc.Text = "İşlem Sonucu";
            tabSonuc.UseVisualStyleBackColor = true;
            // 
            // tabBilgi
            // 
            tabBilgi.Controls.Add(txSonuc);
            tabBilgi.Location = new System.Drawing.Point(4, 22);
            tabBilgi.Name = "tabBilgi";
            tabBilgi.Padding = new Padding(3);
            tabBilgi.Size = new System.Drawing.Size(660, 204);
            tabBilgi.TabIndex = 1;
            tabBilgi.Text = "Bilgi";
            tabBilgi.UseVisualStyleBackColor = true;
            // 
            // txSonuc
            // 
            txSonuc.BackColor = System.Drawing.Color.LightGray;
            txSonuc.Dock = DockStyle.Fill;
            txSonuc.Location = new System.Drawing.Point(3, 3);
            txSonuc.Multiline = true;
            txSonuc.Name = "txSonuc";
            txSonuc.ScrollBars = ScrollBars.Both;
            txSonuc.Size = new System.Drawing.Size(654, 198);
            txSonuc.TabIndex = 1;
            //
            ContextMenuStrip cmenu = new ContextMenuStrip();
            cmenu.Name = "menu";
            cmenu.Items.Add("Kopyala", null, gridKopyala);
            cmenu.Items.Add("Tümünü Seç", null, gridTumSec);
            // 
            // GRD
            // 
            GRD.AllowUserToAddRows = false;
            GRD.AllowUserToDeleteRows = false;
            GRD.AllowUserToOrderColumns = true;
            GRD.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            GRD.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            GRD.Dock = DockStyle.Fill;
            GRD.Location = new System.Drawing.Point(3, 3);
            GRD.Name = "GRD";
            GRD.ReadOnly = true;
            GRD.Size = new System.Drawing.Size(654, 198);
            GRD.TabIndex = 0;
            GRD.ContextMenuStrip = cmenu;
            // 
            // panel1
            // 
            panel1.Controls.Add(btTemizle);
            panel1.Controls.Add(btCalistir);
            panel1.Controls.Add(label1);
            panel1.Dock = DockStyle.Top;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Name = "panel1";
            panel1.Padding = new Padding(4);
            panel1.Size = new System.Drawing.Size(668, 36);
            panel1.TabIndex = 2;
            // 
            // label1
            // 
            label1.Dock = DockStyle.Left;
            label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            label1.ForeColor = System.Drawing.Color.DimGray;
            label1.Location = new System.Drawing.Point(4, 4);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(90, 28);
            label1.TabIndex = 3;
            label1.Text = "Sorgu Çalıştır";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btCalistir
            // 
            btCalistir.Dock = DockStyle.Right;
            btCalistir.Location = new System.Drawing.Point(589, 4);
            btCalistir.Name = "btCalistir";
            btCalistir.Size = new System.Drawing.Size(75, 28);
            btCalistir.TabIndex = 4;
            btCalistir.Text = "Çalıştır";
            btCalistir.UseVisualStyleBackColor = true;
            btCalistir.Click += new EventHandler(sorguBaslat);
            // 
            // btTemizle
            // 
            btTemizle.Dock = DockStyle.Right;
            btTemizle.Location = new System.Drawing.Point(514, 4);
            btTemizle.Name = "btTemizle";
            btTemizle.Size = new System.Drawing.Size(75, 28);
            btTemizle.TabIndex = 5;
            btTemizle.Text = "Temizle";
            btTemizle.UseVisualStyleBackColor = true;
            btTemizle.Click += new EventHandler(sorguTemizle);


            tabControl1.ResumeLayout(false);
            txCommand.ResumeLayout(false);
            panel1.ResumeLayout(false);

        }

        private void gridTumSec(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
            ContextMenuStrip cmenu = (ContextMenuStrip)menuItem.GetCurrentParent();
            DataGridView GRD = (DataGridView)cmenu.SourceControl;
            if (GRD != null) GRD.SelectAll();
        }

        private void gridKopyala(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
            ContextMenuStrip cmenu = (ContextMenuStrip)menuItem.GetCurrentParent();
            DataGridView GRD = (DataGridView)cmenu.SourceControl;
            if (GRD != null)
            {
                GRD.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
                DataObject dataObj = GRD.GetClipboardContent();
                Clipboard.SetDataObject(dataObj, true);
            }
        }

        private void sorguTemizle(object sender, EventArgs e)
        {
            Button BTN = (Button)sender;
            DatabaseInfo dbi2 = (DatabaseInfo)BTN.Parent.Parent.Parent.Tag;
            TextBox txSorgu = (TextBox)nesneBul(BTN.Parent.Parent, "txCommand");
            TabControl TC = (TabControl)nesneBul(BTN.Parent.Parent, "tabControl1");
            TabPage pg1 = TC.TabPages[0];
            TabPage pg2 = TC.TabPages[1];
            DataGridView GRD = (DataGridView)pg1.Controls[0];
            TextBox TxBilgi = (TextBox)pg2.Controls[0];
            GRD.DataSource = null;
            txSorgu.Text = "";
            TxBilgi.Text = "";
        }
        private void sorguBaslat(object sender, EventArgs e)
        {
            Button BTN = (Button)sender;
            DatabaseInfo dbi2 = (DatabaseInfo)BTN.Parent.Parent.Parent.Tag;
            TextBox txSorgu = (TextBox)nesneBul(BTN.Parent.Parent, "txCommand");
            TabControl TC = (TabControl)nesneBul(BTN.Parent.Parent, "tabControl1");
            TabPage pg1 = TC.TabPages[0];
            TabPage pg2 = TC.TabPages[1];
            DataGridView GRD = (DataGridView)pg1.Controls[0];
            TextBox TxBilgi = (TextBox)pg2.Controls[0];
            CultureInfo ci = new CultureInfo("en-EN");
            if (txSorgu.Text.Trim().ToUpper(ci).StartsWith("SELECT") | txSorgu.Text.Trim().ToUpper(ci).StartsWith("EXEC") | txSorgu.Text.Trim().ToUpper(ci).StartsWith("WITH"))
            {
                GRD.DataSource = null;
                DataTable tbListe = new DataTable();
                DataLayer.TabloDoldur(ref tbListe, txSorgu.Text, dbi2);
                if (tbListe != null && tbListe.Columns.Count > 0)
                {
                    GRD.DataSource = tbListe;
                    TC.SelectedIndex = 0;
                    TxBilgi.Text = tbListe.Rows.Count.ToString() + " kayıt listelendi.";
                }
                else
                {
                    TxBilgi.Text = DataLayer.HataMesaj.Message.ToString();
                    TC.SelectedIndex = 1;
                }
            }
            else
            {
                if (DataLayer.SQLCalistir(txSorgu.Text, dbi2))
                {
                    TxBilgi.Text = "Komut Çalıştı";
                    TC.SelectedIndex = 1;
                }
                else
                {
                    TxBilgi.Text = DataLayer.HataMesaj.Message.ToString();
                    TC.SelectedIndex = 1;
                }
            }

        }

        private static void FiyatDegisimObjeCreate(SplitContainer SContainer, DataTable tbFiyatAd)
        {
            DataTable tbKaynakFiyat = tbFiyatAd.Copy();
            DataTable tbHedefFiyat = tbFiyatAd.Copy();

            DataRow yeniKaynakFiyat = tbKaynakFiyat.NewRow();
            yeniKaynakFiyat["ID"] = -1;
            yeniKaynakFiyat["AD"] = "Kaynak Seçilmedi";
            tbKaynakFiyat.Rows.Add(yeniKaynakFiyat);
            tbKaynakFiyat.DefaultView.Sort = "ID ASC";

            DataRow yeniHedeFiyat = tbHedefFiyat.NewRow();
            yeniHedeFiyat["ID"] = -1;
            yeniHedeFiyat["AD"] = "Hedef Seçilmedi";
            tbHedefFiyat.Rows.Add(yeniHedeFiyat);

            DataRow yeniHedefFiyat = tbHedefFiyat.NewRow();
            yeniHedefFiyat["ID"] = 900000000000000000;
            yeniHedefFiyat["AD"] = "Yeni Fiyat Oluştur";
            tbHedefFiyat.Rows.Add(yeniHedefFiyat);
            tbHedefFiyat.DefaultView.Sort = "ID ASC";

            Label label1 = new Label();
            ComboBox cmbKaynakFiyat = new ComboBox();
            Label label2 = new Label();
            ComboBox cmbHedefFiyat = new ComboBox();
            RadioButton rbBosFiyat = new RadioButton();
            RadioButton rbFiyatKopyala = new RadioButton();
            RadioButton rbArtirKopyala = new RadioButton();
            RadioButton rbAzaltKopyala = new RadioButton();
            TextBox txOran = new TextBox();
            Label label3 = new Label();
            CheckBox chkSadeceBosFiyat = new CheckBox();
            Button btnBaslat = new Button();

            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(33, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(87, 13);
            label1.TabIndex = 0;
            label1.Text = "Kaynak Fiyat Adı";
            // 
            // cmbKaynakFiyat
            // 
            cmbKaynakFiyat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cmbKaynakFiyat.FormattingEnabled = true;
            cmbKaynakFiyat.Location = new System.Drawing.Point(126, 6);
            cmbKaynakFiyat.Name = "cmbKaynakFiyat";
            cmbKaynakFiyat.Size = new System.Drawing.Size(237, 21);
            cmbKaynakFiyat.TabIndex = 1;
            cmbKaynakFiyat.SelectedIndexChanged += new System.EventHandler(kaynakFiyatSecildi);
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(39, 36);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(81, 13);
            label2.TabIndex = 2;
            label2.Text = "Hedef Fiyat Adı";
            // 
            // cmbHedefFiyat
            // 
            cmbHedefFiyat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cmbHedefFiyat.FormattingEnabled = true;
            cmbHedefFiyat.Location = new System.Drawing.Point(126, 33);
            cmbHedefFiyat.Name = "cmbHedefFiyat";
            cmbHedefFiyat.Size = new System.Drawing.Size(237, 21);
            cmbHedefFiyat.TabIndex = 3;
            cmbHedefFiyat.SelectedIndexChanged += new System.EventHandler(hedefFiyatSecildi);
            // 
            // rbBosFiyat
            // 
            rbBosFiyat.AutoSize = true;
            rbBosFiyat.Checked = true;
            rbBosFiyat.Location = new System.Drawing.Point(126, 67);
            rbBosFiyat.Name = "rbBosFiyat";
            rbBosFiyat.Size = new System.Drawing.Size(139, 17);
            rbBosFiyat.TabIndex = 11;
            rbBosFiyat.Text = "Boş Fiyat Listesi Oluştur";
            rbBosFiyat.UseVisualStyleBackColor = true;
            rbBosFiyat.CheckedChanged += new System.EventHandler(FiyatIslemTuruSecildi);
            // 
            // rbFiyatKopyala
            // 
            rbFiyatKopyala.AutoSize = true;
            rbFiyatKopyala.Checked = false;
            rbFiyatKopyala.Location = new System.Drawing.Point(126, 90);
            rbFiyatKopyala.Name = "rbFiyatKopyala";
            rbFiyatKopyala.Size = new System.Drawing.Size(63, 17);
            rbFiyatKopyala.TabIndex = 4;
            rbFiyatKopyala.TabStop = true;
            rbFiyatKopyala.Text = "Kopyala";
            rbFiyatKopyala.UseVisualStyleBackColor = true;
            rbFiyatKopyala.CheckedChanged += new System.EventHandler(FiyatIslemTuruSecildi);
            // 
            // rbArtirKopyala
            // 
            rbArtirKopyala.AutoSize = true;
            rbArtirKopyala.Location = new System.Drawing.Point(126, 113);
            rbArtirKopyala.Name = "rbArtirKopyala";
            rbArtirKopyala.Size = new System.Drawing.Size(122, 17);
            rbArtirKopyala.TabIndex = 5;
            rbArtirKopyala.Text = "% Artırarak Kopyala";
            rbArtirKopyala.UseVisualStyleBackColor = true;
            rbArtirKopyala.CheckedChanged += new System.EventHandler(FiyatIslemTuruSecildi);
            // 
            // rbAzaltKopyala
            // 
            rbAzaltKopyala.AutoSize = true;
            rbAzaltKopyala.Location = new System.Drawing.Point(126, 136);
            rbAzaltKopyala.Name = "rbAzaltKopyala";
            rbAzaltKopyala.Size = new System.Drawing.Size(125, 17);
            rbAzaltKopyala.TabIndex = 6;
            rbAzaltKopyala.Text = "% Azaltarak Kopyala";
            rbAzaltKopyala.UseVisualStyleBackColor = true;
            rbAzaltKopyala.CheckedChanged += new System.EventHandler(FiyatIslemTuruSecildi);
            // 
            // txOran
            // 
            txOran.Location = new System.Drawing.Point(126, 159);
            txOran.Name = "txOran";
            txOran.Size = new System.Drawing.Size(88, 21);
            txOran.Enabled = false;
            txOran.TabIndex = 7;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(75, 162);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(45, 13);
            label3.TabIndex = 8;
            label3.Text = "% Oran";
            // 
            // chkSadeceBosFiyat
            // 
            chkSadeceBosFiyat.AutoSize = true;
            chkSadeceBosFiyat.Location = new System.Drawing.Point(126, 186);
            chkSadeceBosFiyat.Name = "chkSadeceBosFiyat";
            chkSadeceBosFiyat.Size = new System.Drawing.Size(126, 17);
            chkSadeceBosFiyat.TabIndex = 9;
            chkSadeceBosFiyat.Text = "Sadece Boş Fiyatlara";
            chkSadeceBosFiyat.Enabled = false;
            chkSadeceBosFiyat.Checked = true;
            chkSadeceBosFiyat.UseVisualStyleBackColor = true;
            // 
            // btnBaslat
            // 
            btnBaslat.Location = new System.Drawing.Point(126, 209);
            btnBaslat.Name = "btnBaslat";
            btnBaslat.Size = new System.Drawing.Size(237, 38);
            btnBaslat.TabIndex = 10;
            btnBaslat.Text = "Başlat";
            btnBaslat.UseVisualStyleBackColor = true;
            btnBaslat.Click += new System.EventHandler(FiyatKopyala);


            SContainer.Panel2.Controls.Add(label1);
            SContainer.Panel2.Controls.Add(label2);
            SContainer.Panel2.Controls.Add(label3);
            SContainer.Panel2.Controls.Add(cmbKaynakFiyat);
            SContainer.Panel2.Controls.Add(cmbHedefFiyat);
            SContainer.Panel2.Controls.Add(rbBosFiyat);
            SContainer.Panel2.Controls.Add(rbFiyatKopyala);
            SContainer.Panel2.Controls.Add(rbArtirKopyala);
            SContainer.Panel2.Controls.Add(rbAzaltKopyala);
            SContainer.Panel2.Controls.Add(chkSadeceBosFiyat);
            SContainer.Panel2.Controls.Add(txOran);
            SContainer.Panel2.Controls.Add(btnBaslat);

            cmbKaynakFiyat.DataSource = tbKaynakFiyat;
            cmbKaynakFiyat.DisplayMember = "AD";
            cmbKaynakFiyat.ValueMember = "ID";

            cmbHedefFiyat.DataSource = tbHedefFiyat;
            cmbHedefFiyat.DisplayMember = "AD";
            cmbHedefFiyat.ValueMember = "ID";



        }

        private static void kaynakFiyatSecildi(object sender, EventArgs e)
        {
            ComboBox CMB = (ComboBox)sender;
            RadioButton RB1 = (RadioButton)nesneBul(CMB.Parent, "rbBosFiyat");
            RadioButton RB2 = (RadioButton)nesneBul(CMB.Parent, "rbFiyatKopyala");
            RadioButton RB3 = (RadioButton)nesneBul(CMB.Parent, "rbArtirKopyala");
            RadioButton RB4 = (RadioButton)nesneBul(CMB.Parent, "rbAzaltKopyala");

            if (CMB.SelectedIndex == 0)
            {
                RB1.Enabled = false;
                RB2.Enabled = false;
                RB3.Enabled = false;
                RB4.Enabled = false;
                RB1.Checked = true;
            }
            else
            {
                RB1.Enabled = true;
                RB2.Enabled = true;
                RB3.Enabled = true;
                RB4.Enabled = true;
            }

        }
        private static void FiyatKopyala(object sender, EventArgs e)
        {
            Button BTN = (Button)sender;
            DatabaseInfo dbi2 = (DatabaseInfo)BTN.Parent.Parent.Tag;
            ComboBox CMK = (ComboBox)nesneBul(BTN.Parent, "cmbKaynakFiyat");
            ComboBox CMH = (ComboBox)nesneBul(BTN.Parent, "cmbHedefFiyat");
            RadioButton RB1 = (RadioButton)nesneBul(BTN.Parent, "rbBosFiyat");
            RadioButton RB2 = (RadioButton)nesneBul(BTN.Parent, "rbFiyatKopyala");
            RadioButton RB3 = (RadioButton)nesneBul(BTN.Parent, "rbArtirKopyala");
            RadioButton RB4 = (RadioButton)nesneBul(BTN.Parent, "rbAzaltKopyala");
            TextBox Tx = (TextBox)nesneBul(BTN.Parent, "txOran");
            CheckBox Chk = (CheckBox)nesneBul(BTN.Parent, "chkSadeceBosFiyat");

            if (CMH.SelectedIndex == 0)
            {
                MessageBox.Show("Hedef Fiyat Seçmelisiniz");
                return;
            }
            Int32 kaynakFiyatId; if (!Int32.TryParse(CMK.SelectedValue.ToString(), out kaynakFiyatId)) kaynakFiyatId = 0;
            Int32 hedefFiyatId; if (!Int32.TryParse(CMH.SelectedValue.ToString(), out hedefFiyatId)) hedefFiyatId = 0;

            String Sorgular = String.Empty;
            /* Boş Kopyala */
            if (RB1.Checked)
            {
                Int32 sonFiyatd = DataLayer.SQLInt32("SELECT MAX(ID) FROM STOK_STOK_BIRIM_FIYAT", 0, dbi2);
                Sorgular = @"INSERT INTO STOK_STOK_BIRIM_FIYAT (ID,STOK_STOK_BIRIM,DOVIZ_AD,STOK_FIYAT_AD,FIYAT,KDV_DAHILMI) SELECT NEXT VALUE FOR ID, SSB.ID,1," + hedefFiyatId.ToString() + ",0,1 FROM STOK S INNER JOIN STOK_STOK_BIRIM SSB ON SSB.STOK=S.ID AND SSB.VARSAYILAN=1 WHERE SSB.ID NOT IN (SELECT STOK_STOK_BIRIM FROM STOK_STOK_BIRIM_FIYAT WHERE STOK_FIYAT_AD=" + hedefFiyatId + ")";
                if (DataLayer.SQLCalistir(Sorgular, dbi2))
                {
                    Sorgular = "INSERT INTO SEQUNCES_DEGISIKLIK (KOD_PC,SEQUNCES,KULLANICI,ISLEM,TARIH,TABLO) SELECT 100,ID,437809,1,GETDATE(),'STOK_STOK_BIRIM_FIYAT' FROM STOK_STOK_BIRIM_FIYAT WHERE ID>" + sonFiyatd.ToString() + " STOK_FIYAT_AD=" + hedefFiyatId.ToString();
                    DataLayer.SQLCalistir(Sorgular, dbi2);
                    MessageBox.Show("İşlem Tamamlandı", "TAMAM", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    return;
                }
            }
            /* Direkt Kopyala */
            if (RB2.Checked)
            {
                Int32 sonFiyatId = DataLayer.SQLInt32("SELECT MAX(ID) FROM STOK_STOK_BIRIM_FIYAT", 0, dbi2);
                Sorgular = @"INSERT INTO STOK_STOK_BIRIM_FIYAT (ID,STOK_STOK_BIRIM,DOVIZ_AD,STOK_FIYAT_AD,FIYAT,KDV_DAHILMI) 
SELECT NEXT VALUE FOR ID,F.STOK_STOK_BIRIM,F.DOVIZ_AD," + hedefFiyatId.ToString() + @",F.FIYAT,F.KDV_DAHILMI 
FROM STOK_STOK_BIRIM_FIYAT F
WHERE F.STOK_FIYAT_AD=" + kaynakFiyatId.ToString() + @" AND F.STOK_STOK_BIRIM NOT IN (SELECT STOK_STOK_BIRIM FROM STOK_STOK_BIRIM_FIYAT WHERE STOK_FIYAT_AD=" + hedefFiyatId.ToString() + @")";
                if (DataLayer.SQLCalistir(Sorgular, dbi2))
                {
                    Sorgular = "INSERT INTO SEQUNCES_DEGISIKLIK (KOD_PC,SEQUNCES,KULLANICI,ISLEM,TARIH,TABLO) SELECT 100,ID,437809,1,GETDATE(),'STOK_STOK_BIRIM_FIYAT' FROM STOK_STOK_BIRIM_FIYAT WHERE ID>" + sonFiyatId.ToString() + " STOK_FIYAT_AD=" + hedefFiyatId.ToString();
                    DataLayer.SQLCalistir(Sorgular, dbi2);

                    if (!Chk.Checked)
                    {
                        Sorgular = @"UPDATE HF SET HF.FIYAT=KF.FIYAT,HF.DOVIZ_AD=KF.DOVIZ_AD,HF.KDV_DAHILMI=KF.KDV_DAHILMI
FROM STOK_STOK_BIRIM_FIYAT HF
INNER JOIN STOK_STOK_BIRIM_FIYAT KF ON KF.STOK_STOK_BIRIM=HF.STOK_STOK_BIRIM
WHERE HF.STOK_FIYAT_AD=" + hedefFiyatId.ToString() + " AND KF.STOK_FIYAT_AD=" + kaynakFiyatId.ToString();
                        if (DataLayer.SQLCalistir(Sorgular, dbi2))
                        {
                            Sorgular = "INSERT INTO SEQUNCES_DEGISIKLIK (KOD_PC,SEQUNCES,KULLANICI,ISLEM,TARIH,TABLO) SELECT 100,HF.ID,437809,2,GETDATE(),'STOK_STOK_BIRIM_FIYAT' FROM STOK_STOK_BIRIM_FIYAT HF INNER JOIN STOK_STOK_BIRIM_FIYAT KF ON KF.STOK_STOK_BIRIM=HF.STOK_STOK_BIRIM WHERE HF.STOK_FIYAT_AD=" + hedefFiyatId.ToString() + " AND KF.STOK_FIYAT_AD=" + kaynakFiyatId.ToString();
                            DataLayer.SQLCalistir(Sorgular, dbi2);
                            MessageBox.Show("İşlem Tamamlandı", "TAMAM", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            return;
                        }
                    }
                    MessageBox.Show("İşlem Tamamlandı", "TAMAM", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    return;
                }

            }
            /* Artır Kopyala */
            if (RB3.Checked)
            {
                Int32 sonFiyatd = DataLayer.SQLInt32("SELECT MAX(ID) FROM STOK_STOK_BIRIM_FIYAT", 0, dbi2);
                Decimal artisOran;
                if (!Decimal.TryParse(Tx.Text, out artisOran)) artisOran = 0;
                Decimal hesapOran = ((100 + artisOran) / 100);
                Sorgular = @"INSERT INTO STOK_STOK_BIRIM_FIYAT (ID,STOK_STOK_BIRIM,DOVIZ_AD,STOK_FIYAT_AD,FIYAT,KDV_DAHILMI) 
SELECT NEXT VALUE FOR ID,F.STOK_STOK_BIRIM,F.DOVIZ_AD," + hedefFiyatId.ToString() + @",(F.FIYAT)*" + DataLayer.RakamSQL(hesapOran) + @",F.KDV_DAHILMI 
FROM STOK_STOK_BIRIM_FIYAT F
WHERE F.STOK_FIYAT_AD=" + kaynakFiyatId.ToString() + @" AND F.STOK_STOK_BIRIM NOT IN (SELECT STOK_STOK_BIRIM FROM STOK_STOK_BIRIM_FIYAT WHERE STOK_FIYAT_AD=" + hedefFiyatId.ToString() + @")";
                if (DataLayer.SQLCalistir(Sorgular, dbi2))
                {
                    Sorgular = "INSERT INTO SEQUNCES_DEGISIKLIK (KOD_PC,SEQUNCES,KULLANICI,ISLEM,TARIH,TABLO) SELECT 100,ID,437809,1,GETDATE(),'STOK_STOK_BIRIM_FIYAT' FROM STOK_STOK_BIRIM_FIYAT WHERE ID>" + sonFiyatd.ToString() + " STOK_FIYAT_AD=" + hedefFiyatId.ToString();
                    DataLayer.SQLCalistir(Sorgular, dbi2);
                    if (!Chk.Checked)
                    {
                        Sorgular = @"UPDATE HF SET HF.FIYAT=KF.FIYAT*" + DataLayer.RakamSQL(hesapOran) + @",HF.DOVIZ_AD=KF.DOVIZ_AD,HF.KDV_DAHILMI=KF.KDV_DAHILMI
FROM STOK_STOK_BIRIM_FIYAT HF
INNER JOIN STOK_STOK_BIRIM_FIYAT KF ON KF.STOK_STOK_BIRIM=HF.STOK_STOK_BIRIM
WHERE HF.STOK_FIYAT_AD=" + hedefFiyatId.ToString() + " AND KF.STOK_FIYAT_AD=" + kaynakFiyatId.ToString();
                        if (DataLayer.SQLCalistir(Sorgular, dbi2))
                        {
                            Sorgular = "INSERT INTO SEQUNCES_DEGISIKLIK (KOD_PC,SEQUNCES,KULLANICI,ISLEM,TARIH,TABLO) SELECT 100,HF.ID,437809,2,GETDATE(),'STOK_STOK_BIRIM_FIYAT' FROM STOK_STOK_BIRIM_FIYAT HF INNER JOIN STOK_STOK_BIRIM_FIYAT KF ON KF.STOK_STOK_BIRIM=HF.STOK_STOK_BIRIM WHERE HF.STOK_FIYAT_AD=" + hedefFiyatId.ToString() + " AND KF.STOK_FIYAT_AD=" + kaynakFiyatId.ToString();
                            DataLayer.SQLCalistir(Sorgular, dbi2);
                            MessageBox.Show("İşlem Tamamlandı", "TAMAM", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            return;
                        }
                    }
                    MessageBox.Show("İşlem Tamamlandı", "TAMAM", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    return;
                }

            }
            /* Azalt Kopyala */
            if (RB4.Checked)
            {
                Int32 sonFiyatd = DataLayer.SQLInt32("SELECT MAX(ID) FROM STOK_STOK_BIRIM_FIYAT", 0, dbi2);
                Decimal artisOran;
                if (!Decimal.TryParse(Tx.Text, out artisOran)) artisOran = 0;
                Decimal hesapOran = ((100 - artisOran) / 100);
                Sorgular = @"INSERT INTO STOK_STOK_BIRIM_FIYAT (ID,STOK_STOK_BIRIM,DOVIZ_AD,STOK_FIYAT_AD,FIYAT,KDV_DAHILMI) 
SELECT NEXT VALUE FOR ID,F.STOK_STOK_BIRIM,F.DOVIZ_AD," + hedefFiyatId.ToString() + @",(F.FIYAT)*" + DataLayer.RakamSQL(hesapOran) + @",F.KDV_DAHILMI 
FROM STOK_STOK_BIRIM_FIYAT F
WHERE F.STOK_FIYAT_AD=" + kaynakFiyatId.ToString() + @" AND F.STOK_STOK_BIRIM NOT IN (SELECT STOK_STOK_BIRIM FROM STOK_STOK_BIRIM_FIYAT WHERE STOK_FIYAT_AD=" + hedefFiyatId.ToString() + @")";
                if (DataLayer.SQLCalistir(Sorgular, dbi2))
                {
                    if (!Chk.Checked)
                    {
                        Sorgular = "INSERT INTO SEQUNCES_DEGISIKLIK (KOD_PC,SEQUNCES,KULLANICI,ISLEM,TARIH,TABLO) SELECT 100,ID,437809,1,GETDATE(),'STOK_STOK_BIRIM_FIYAT' FROM STOK_STOK_BIRIM_FIYAT WHERE ID>" + sonFiyatd.ToString() + " STOK_FIYAT_AD=" + hedefFiyatId.ToString();
                        DataLayer.SQLCalistir(Sorgular, dbi2);
                        Sorgular = @"UPDATE HF SET HF.FIYAT=KF.FIYAT*" + DataLayer.RakamSQL(hesapOran) + @",HF.DOVIZ_AD=KF.DOVIZ_AD,HF.KDV_DAHILMI=KF.KDV_DAHILMI
FROM STOK_STOK_BIRIM_FIYAT HF
INNER JOIN STOK_STOK_BIRIM_FIYAT KF ON KF.STOK_STOK_BIRIM=HF.STOK_STOK_BIRIM
WHERE HF.STOK_FIYAT_AD=" + hedefFiyatId.ToString() + " AND KF.STOK_FIYAT_AD=" + kaynakFiyatId.ToString();
                        if (DataLayer.SQLCalistir(Sorgular, dbi2))
                        {
                            Sorgular = "INSERT INTO SEQUNCES_DEGISIKLIK (KOD_PC,SEQUNCES,KULLANICI,ISLEM,TARIH,TABLO) SELECT 100,HF.ID,437809,2,GETDATE(),'STOK_STOK_BIRIM_FIYAT' FROM STOK_STOK_BIRIM_FIYAT HF INNER JOIN STOK_STOK_BIRIM_FIYAT KF ON KF.STOK_STOK_BIRIM=HF.STOK_STOK_BIRIM WHERE HF.STOK_FIYAT_AD=" + hedefFiyatId.ToString() + " AND KF.STOK_FIYAT_AD=" + kaynakFiyatId.ToString();
                            DataLayer.SQLCalistir(Sorgular, dbi2);
                            MessageBox.Show("İşlem Tamamlandı", "TAMAM", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            return;
                        }
                    }
                    MessageBox.Show("İşlem Tamamlandı", "TAMAM", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    return;
                }

            }


        }

        private static void FiyatIslemTuruSecildi(object sender, EventArgs e)
        {
            RadioButton RB = (RadioButton)sender;
            TextBox Tx = (TextBox)nesneBul(RB.Parent, "txOran");
            CheckBox Ch = (CheckBox)nesneBul(RB.Parent, "chkSadeceBosFiyat");
            if (RB.Name == "rbBosFiyat")
            {
                Ch.Enabled = false;
                Ch.Checked = true;
                Tx.Enabled = false;
                Tx.Text = "";
            }
            else if (RB.Name == "rbFiyatKopyala")
            {
                Ch.Enabled = true;
                Ch.Checked = true;
                Tx.Enabled = false;
                Tx.Text = "";
            }
            else
            {
                Ch.Enabled = true;
                Ch.Checked = true;
                Tx.Enabled = true;
                Tx.Text = "0";
            }
        }
        private static void hedefFiyatSecildi(object sender, EventArgs e)
        {

            ComboBox CMB = (ComboBox)sender;

            if (CMB.SelectedIndex > -1)
            {
                if (CMB.SelectedValue.ToString().Equals("900000000000000000"))
                {
                    String yeniFiyatAd = Interaction.InputBox("Yeni Fiyat Adı", "Fiyat Tanımla");
                    if (yeniFiyatAd.Trim().Equals(String.Empty)) return;
                    DataTable TBL = (DataTable)CMB.DataSource;
                    DataRow[] RW = TBL.Select("AD='" + yeniFiyatAd + "'");
                    if (RW.Length > 0)
                    {
                        MessageBox.Show("Fiyat Adı Zaten Var");
                        return;
                    }
                    DatabaseInfo dbi2 = (DatabaseInfo)CMB.Parent.Parent.Tag;
                    Int32 yeniFiyatId = DataLayer.SQLSequences("STOK_FIYAT_AD", dbi2);
                    if (DataLayer.SQLCalistir("INSERT INTO STOK_FIYAT_AD (ID,AD,KDV_DAHILMI) VALUES (" + yeniFiyatId.ToString() + ",'" + yeniFiyatAd + "',1)", dbi2))
                    {
                        DataRow nRW = TBL.NewRow();
                        nRW["ID"] = yeniFiyatId;
                        nRW["AD"] = yeniFiyatAd;
                        TBL.Rows.Add(nRW);
                    }
                }
            }
        }

    }
}
