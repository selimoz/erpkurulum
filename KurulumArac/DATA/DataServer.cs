﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KurulumArac
{
    class DataServer
    {
        private String serverName;
        private String userName;
        private String password;
        private Boolean baglantiVar;
        public List<String> dbCatalaog = new List<String>();
        public string ServerName
        {
            get => serverName; set
            {
                serverName = value;
                baglantiVar = false;
            }
        }
        public string UserName
        {
            get => userName; set
            {
                userName = value;
                baglantiVar = false;
            }
        }
        public string Password
        {
            get => password; set
            {
                password = value;
                baglantiVar = false;
            }
        }
        public bool BaglantiVar { get => baglantiVar; }


        public DataServer()
        {
            serverName = ".";
            userName = "sa";
            password = "";
            baglantiVar = false;
        }
        public Boolean BaglantiDene()
        {
            DatabaseInfo DI = new DatabaseInfo();
            DI.Host = serverName;
            DI.Userid = userName;
            DI.Password = password;
            DI.DataCatalog = "master";
            DI.ConnectionStringCreate();
            if (DataLayer.BaglantiTest(DI))
            {
                baglantiVar = true;
                CatalogBul();
            }
            else baglantiVar = false;
            return BaglantiVar;
        }
        public void CatalogBul()
        {
            if (BaglantiVar)
            {
                DataTable TB = new DataTable();
                DataLayer.TabloDoldur(ref TB, "SELECT * FROM sys.databases where LOWER(name) not in ('master','tempdb','model','msdb')", new DatabaseInfo(DatabaseInfo.databasetype.MsSQL, serverName, userName, password, "master"));
                if (TB != null && TB.Rows.Count > 0)
                {
                    dbCatalaog.Clear();
                    foreach (DataRow R in TB.Rows)
                    {
                        dbCatalaog.Add(R["name"].ToString());
                    }
                }

            }

        }

    }
}
