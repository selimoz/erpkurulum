﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace KurulumArac
{
    class DataLayer
    {
        public static Exception HataMesaj;
        public static String Ondalik = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;
        public static List<DataKayit> SQListe = new List<DataKayit>();
        public static DatabaseInfo AktifDB = new DatabaseInfo();
        public static DatabaseInfo masterDB = new DatabaseInfo();
        public static String YeniDataAdi = String.Empty;
        public static Hashtable htTablolar = new Hashtable();

        public static void HashDoldur(ref Hashtable HTB, String Komut, DatabaseInfo DBI)
        {
            DataTable DT = new DataTable();
            if (HTB == null) HTB = new Hashtable();
            HTB.Clear();
            TabloDoldur(ref DT, Komut, DBI);
            if (DT != null && DT.Columns.Count > 0 && DT.Rows.Count > 0)
            {
                foreach (DataRow RW in DT.Rows)
                {
                    HTB.Add(RW[0], RW[1]);
                }
            }
            else
            {
                HTB = null;
            }

        }
        public static void TabloDoldur(ref DataTable TB, String Komut, DatabaseInfo DBI)
        {
            if (TB == null) TB = new DataTable();
            try
            {
                using (SqlConnection sCon = new SqlConnection(DBI.ConnectionString))
                {

                    using (SqlCommand sCmd = new SqlCommand(Komut, sCon))
                    {
                        sCmd.CommandTimeout = 300;
                        SqlDataAdapter da = new SqlDataAdapter(sCmd);
                        da.Fill(TB);
                    }
                }
            }
            catch (Exception DLHata)
            {
                HataMesaj = DLHata;
                TB = null;
            }
        }
        public static String SQLKolonlar(String Komut, DatabaseInfo DBI)
        {
            String kolon = String.Empty;
            DataTable table = new DataTable();
            try
            {
                using (SqlConnection sCon = new SqlConnection(DBI.ConnectionString))
                {

                    using (SqlCommand sCmd = new SqlCommand(Komut, sCon))
                    {
                        sCmd.CommandTimeout = 300;
                        SqlDataAdapter da = new SqlDataAdapter(sCmd);
                        da.Fill(table);
                        if (table != null && table.Columns.Count > 0)
                        {
                            foreach (DataColumn Col in table.Columns)
                            {
                                if (kolon == "") kolon = Col.ColumnName; else kolon += "," + Col.ColumnName;
                            }
                            return kolon;
                        }
                    }
                }
            }
            catch (Exception DLHata)
            {
                HataMesaj = DLHata;
            }
            return kolon;
        }
        public static Int32 SQLSequences(String TabloAdi, DatabaseInfo DBI)
        {
            String Komut = "EXEC SEQUENS_VER @TABLO='" + TabloAdi + "', @KOD_PC=1212";

            try
            {
                using (SqlConnection sCon = new SqlConnection(DBI.ConnectionString))
                {
                    using (SqlCommand sCMD = new SqlCommand(Komut, sCon))
                    {
                        sCon.Open();
                        Int32 Deger = Int32.Parse(sCMD.ExecuteScalar().ToString());
                        SQListe.Add(new DataKayit(TabloAdi, Deger.ToString()));
                        return Deger;
                    }
                }
            }
            catch (Exception DLHata)
            {
                HataMesaj = DLHata;
                return -1;
            }
        }
        public static Int32 SQLInt32(String Komut, Int32 Varsayilan, DatabaseInfo DBI)
        {
            try
            {
                using (SqlConnection sCon = new SqlConnection(DBI.ConnectionString))
                {
                    using (SqlCommand sCMD = new SqlCommand(Komut, sCon))
                    {
                        sCon.Open();
                        SqlDataReader RD = sCMD.ExecuteReader();
                        while (RD.Read())
                        {
                            Int32 Deger = 0;
                            if (Int32.TryParse(RD[0].ToString(), out Deger))
                            {
                                return Deger;
                            }
                            else
                            {
                                return Varsayilan;
                            }
                        }
                        return Varsayilan;
                    }
                }
            }
            catch (Exception DLHata)
            {
                HataMesaj = DLHata;
                return Varsayilan;
            }
        }
        public static Decimal SQLDecimal(String Komut, Decimal Varsayilan, DatabaseInfo DBI)
        {
            try
            {
                using (SqlConnection sCon = new SqlConnection(DBI.ConnectionString))
                {
                    using (SqlCommand sCMD = new SqlCommand(Komut, sCon))
                    {
                        sCon.Open();
                        SqlDataReader RD = sCMD.ExecuteReader();
                        while (RD.Read())
                        {
                            return RD.GetDecimal(0);
                        }
                        return Varsayilan;
                    }
                }
            }
            catch (Exception DLHata)
            {
                HataMesaj = DLHata;
                return Varsayilan;
            }
        }
        public static Boolean SQLCalistir(String Komut, DatabaseInfo DBI)
        {
            try
            {
                using (SqlConnection sCon = new SqlConnection(DBI.ConnectionString))
                {
                    using (SqlCommand sCMD = new SqlCommand(Komut, sCon))
                    {
                        sCon.Open();
                        sCMD.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch (SqlException DHata)
            {
                HataMesaj = DHata;
                return false;
            }
            catch (Exception DLHata)
            {
                HataMesaj = DLHata;
                return false;
            }

        }
        public static String SQLString(String Komut, String Varsayilan, DatabaseInfo DBI)
        {
            try
            {
                using (SqlConnection sCon = new SqlConnection(DBI.ConnectionString))
                {
                    using (SqlCommand sCMD = new SqlCommand(Komut, sCon))
                    {
                        sCon.Open();
                        SqlDataReader RD = sCMD.ExecuteReader();
                        while (RD.Read())
                        {
                            String Deger = RD.GetString(0);
                            return Deger;
                        }
                        return Varsayilan;
                    }
                }
            }
            catch (Exception DLHata)
            {
                HataMesaj = DLHata;
                return Varsayilan;
            }
        }
        public static Int32 TabloIdBul(String TabloAdi)
        {
            Object HH = DataLayer.htTablolar[TabloAdi];
            if (HH == null)
            {
                return -1;
            }
            else
            {
                return Int32.Parse(HH.ToString());
            }

        }
        public static String TirnakTemizle(String Metin)
        {
            if (Metin.Length > 1)
            {
                String Ilk = Metin.Substring(0, 1);
                String Son = Metin.Substring(Metin.Length - 1, 1);
                if (Ilk == "'") Metin = Metin.Substring(1, Metin.Length - 1);
                if (Son == "'") Metin = Metin.Substring(0, Metin.Length - 1);
                return Metin;
            }
            else
            {
                return Metin.Replace("'", "");
            }
        }
        public static Boolean BaglantiTest(DatabaseInfo DBI)
        {
            try
            {
                using (SqlConnection scon = new SqlConnection(DBI.ConnectionString))
                {
                    scon.Open();
                }
                return true;
            }
            catch (Exception Ext)
            {
                HataMesaj = Ext;
                return false;
            }

        }
        public static String RakamSQL(Decimal Rakam)
        {
            String RakamS = RakamTemizle(Rakam.ToString("#0.#0000000").Replace(",", "."));
            return RakamS;
        }
        public static String RakamTemizle(String Rakam)
        {
            if (Rakam == "") return "";
            for (int i = Rakam.Length - 1; i > 0; i--)
            {
                String HS = Rakam.Substring(i, 1);
                int HR = 0;
                if (!int.TryParse(HS, out HR) || HS == ".")
                {
                    return Rakam.Substring(0, i);
                }
                else
                {
                    if (HR > 0) return Rakam.Substring(0, i + 1);
                }
            }
            return Rakam;
        }
        public static Int32 IdBulDataTable(DataTable TB, String Alan, String Deger)
        {
            if (TB != null && TB.Columns.Contains("ID") && TB.Columns.Contains(Alan) && TB.Rows.Count > 0)
            {
                DataRow[] R = TB.Select(Alan + "=" + Deger);
                if (R.Length > 0)
                {
                    return Int32.Parse(R[0]["ID"].ToString());
                }
                else return -1;
            }
            else return -1;
        }
        public static Int32 IdBulDataTable(DataTable TB, String Sart)
        {
            try
            {
                DataRow[] R = TB.Select(Sart);
                if (R.Length > 0)
                {
                    return Int32.Parse(R[0]["ID"].ToString());
                }
                else return -1;
            }
            catch (Exception Ex)
            {
                HataMesaj = Ex;
                return -1;
            }

        }
        public static String RakamSQL(String Rakam, String Varsayilan = "0")
        {
            Decimal RakamDec = 0;
            if (Rakam == "") return Varsayilan;
            if (Decimal.TryParse(Rakam, out RakamDec))
            {
                return RakamSQL(RakamDec);
            }
            else return Varsayilan;
        }
        public static string TelFormat(String TEL)
        {
            if (TEL == "" | TEL == "''") return TEL;
            String TTemp = String.Empty;
            String NL = "0123456789";
            TEL = TEL.Replace("'", "");
            int Sayac = 0;
            for (int i = TEL.Length - 1; i >= 0; i--)
            {
                String H = TEL.Substring(i, 1);
                if (NL.IndexOf(H) >= 0)
                {
                    Sayac++;
                    if (Sayac < 5)
                    {
                        TTemp = H + TTemp;
                    }
                    else if (Sayac == 5)
                    {
                        TTemp = H + "-" + TTemp;
                    }
                    else if (Sayac > 5 & Sayac < 8)
                    {
                        TTemp = H + TTemp;
                    }
                    else if (Sayac == 8)
                    {
                        TTemp = H + ") " + TTemp;
                    }
                    else if (Sayac < 10)
                    {
                        TTemp = H + TTemp;
                    }
                    if (Sayac == 10)
                    {
                        TTemp = "(" + H + TTemp;
                    }
                    if (Sayac > 10)
                    {
                        continue;
                    }
                }

            }
            if (TTemp.Length < 5)
            {
                TTemp = "() -" + TTemp;
            }
            else if (TTemp.Length < 8)
            {
                TTemp = "() " + TTemp;
            }
            else if (TTemp.Length == 9)
            {
                TTemp = "()" + TTemp;
            }
            else if (TTemp.Length >= 10 & TTemp.Length <= 13)
            {
                TTemp = "(" + TTemp;
            }
            return TTemp;
        }
        public static void SQTemizle()
        {
            SQListe.Clear();
        }
        public static String YedekAl(DatabaseInfo DI)
        {
            String YedekDosya = Application.StartupPath + @"\" + DI.DataCatalog + @"_" + DateTime.Now.ToString("yyyy-MM-dd_HHmm") + ".Bak";
            String Komut = "BACKUP DATABASE [" + DI.DataCatalog + "] TO  DISK = N'" + YedekDosya + "' WITH NOFORMAT, NOINIT,  NAME = N'" + DI.DataCatalog + "-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";
            if (!SQLCalistir(Komut, DI))
            {
                MessageBox.Show("Yedek Alınamadı ... ! \r\n" + HataMesaj.ToString());
                return "";
            }
            else
            {
                return YedekDosya;
            }
        }
        public static DateTime EmexTarih(String Tarih)
        {
            if (Tarih == "") Tarih = DateTime.Now.ToString("yyyyMMdd");
            String TRH = Tarih.Substring(0, 4);
            TRH += "-" + Tarih.Substring(4, 2);
            TRH += "-" + Tarih.Substring(Tarih.Length - 2, 2);
            DateTime DTarih;
            if (DateTime.TryParse(TRH, out DTarih))
            {
                return DTarih;
            }
            else return DTarih;
        }
        public static String IskMetin(String Metin)
        {
            String[] Mevcut = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "." };
            String Sonuc = String.Empty;
            for (int i = 0; i < Metin.Length; i++)
            {
                String H = Metin.Substring(i, 1);
                if (Mevcut.Contains(H))
                {
                    Sonuc += H;
                }
            }
            return Sonuc;
        }
        public static Boolean RaporUserTemizle()
        {
            if (masterDB == null)
            {
                MessageBox.Show("Veritabanı Bağlantısını Kontrol Ediniz.");
                return false;
            }
            masterDB.ConnectionStringCreate();

            String Komut = "use master \r\n";
            Komut += "DECLARE @dbname VARCHAR(50) \r\n  ";
            Komut += "DECLARE @statement NVARCHAR(max) \r\n";
            Komut += "DECLARE db_cursor CURSOR \r\n";
            Komut += "LOCAL FAST_FORWARD \r\n";
            Komut += "FOR \r\n";
            Komut += "SELECT name \r\n";
            Komut += "FROM MASTER.dbo.sysdatabases \r\n";
            Komut += "WHERE name NOT IN('master', 'model', 'msdb', 'tempdb', 'distribution') \r\n";
            Komut += "OPEN db_cursor \r\n";
            Komut += "FETCH NEXT FROM db_cursor INTO @dbname \r\n";
            Komut += "WHILE @@FETCH_STATUS = 0 \r\n";
            Komut += "BEGIN \r\n";
            Komut += "SELECT @statement = 'use ' + @dbname + ';' + 'DROP USER [rapor];' \r\n";
            Komut += "exec sp_executesql @statement \r\n";
            Komut += "FETCH NEXT FROM db_cursor INTO @dbname \r\n";
            Komut += "END \r\n";
            Komut += "CLOSE db_cursor \r\n";
            Komut += "DEALLOCATE db_cursor ";

            if (SQLCalistir(Komut, masterDB))
            {

            }
            if (SQLCalistir("DROP LOGIN [rapor]", masterDB))
            {

            }
            return false;

        }
        public static Boolean RaporUserEkle()
        {
            if (masterDB == null)
            {
                MessageBox.Show("Veritabanı Bağlantısını Kontrol Ediniz.");
                return false;
            }
            //masterDB.ConnectionStringCreate();
            String Komut = "if (Not Exists(select TOP 1 1 from sys.syslogins Where name='rapor'))\r\n";
            Komut += "Begin \r\n";
            Komut += "    CREATE LOGIN[rapor] WITH PASSWORD = N')hicXC+fme.3LBpo', DEFAULT_DATABASE =[master], CHECK_EXPIRATION = OFF, CHECK_POLICY = ON \r\n";
            Komut += "End";

            if (SQLCalistir(Komut, masterDB))
            {
                Komut = "DECLARE @dbname VARCHAR(50)  \r\n ";
                Komut += "DECLARE @statement NVARCHAR(max) \r\n";
                Komut += "DECLARE db_cursor CURSOR   \r\n";
                Komut += "LOCAL FAST_FORWARD  \r\n";
                Komut += "FOR \r\n";
                Komut += "SELECT name  \r\n";
                Komut += "FROM MASTER.dbo.sysdatabases  \r\n";
                Komut += "WHERE name NOT IN ('master','model','msdb','tempdb','distribution')  \r\n";
                Komut += "OPEN db_cursor   \r\n";
                Komut += "FETCH NEXT FROM db_cursor INTO @dbname    \r\n";
                Komut += "WHILE @@FETCH_STATUS = 0  \r\n";
                Komut += "BEGIN  \r\n";
                Komut += "SELECT @statement = 'use '+@dbname +';'+ 'DROP USER [rapor]; CREATE USER [rapor]   \r\n";
                Komut += "FOR LOGIN [rapor]; \r\n";
                Komut += " EXEC sp_addrolemember N''db_datareader'', [rapor]'  \r\n";
                //Komut += "EXEC sp_addrolemember N''db_datawriter'', [rapor]'  \r\n";
                Komut += "exec sp_executesql @statement  \r\n";
                Komut += "FETCH NEXT FROM db_cursor INTO @dbname  \r\n";
                Komut += "END  \r\n";
                Komut += "CLOSE db_cursor    \r\n";
                Komut += "DEALLOCATE db_cursor   \r\n";
                Komut += "  \r\n";

                if (SQLCalistir(Komut, masterDB))
                {
                    return true;
                }
                else return false;

            }
            return false;
        }
        public static String SQLUserPasswordChange(String pHost, String pUser)
        {
            SqlKur Sq = new SqlKur(Application.StartupPath.ToString());
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = pHost;
            csb.IntegratedSecurity = true;
            csb.InitialCatalog = "master";
            String YeniParola = Sq.ParolaUret();
            using (SqlConnection scn = new SqlConnection(csb.ConnectionString))
            {
                try
                {
                    scn.Open();
                    Geri:
                    YeniParola = Interaction.InputBox("Yeni Parolayı Giriniz : ", "SQL Parola Değiştir", YeniParola);
                    if (YeniParola == "")
                    {
                        goto Geri;
                    }
                    using (SqlCommand cmd = new SqlCommand("ALTER LOGIN [" + pUser + "] WITH PASSWORD=N'" + YeniParola + "'", scn))
                    {
                        cmd.ExecuteNonQuery();
                        return YeniParola;
                    }
                }
                catch (SqlException sExc)
                {

                    MessageBox.Show(sExc.Message.ToString());
                    return YeniParola;
                }

            }




        }
    }
}
