﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Microsoft.Win32;

namespace KurulumArac
{
    class DatabaseInfo
    {
        public enum databasetype
        {
            MsSQL, Firebird, MySQL
        }

        internal String Text = "";

        private string databaseName;
        private databasetype databaseType;
        private String host;
        private Int16 port;
        private String userid;
        private String password;
        private String dataCatalog;
        private String connectionString;


        public string DataBaseName { get => databaseName; set => databaseName = value; }
        internal databasetype DatabaseType { get => databaseType; set => databaseType = value; }
        public string Host { get => host; set => host = value; }
        public short Port { get => port; set => port = value; }
        public string Userid { get => userid; set => userid = value; }
        public string Password { get => password; set => password = value; }
        public string DataCatalog { get => dataCatalog; set => dataCatalog = value; }
        public string ConnectionString
        {
            get => connectionString;
            set
            {
                connectionString = value;
                ConnectionStringtoDetail();
            }
        }


        public DatabaseInfo()
        {
            databaseType = databasetype.MsSQL;
        }
        public DatabaseInfo(databasetype tip)
        {
            databaseType = tip;
        }
        public DatabaseInfo(databasetype DBType, String HostName, String UserId, String PassWord, String dataCalalog)
        {
            this.databaseType = DBType;
            this.host = HostName;
            this.userid = UserId;
            this.password = PassWord;
            this.dataCatalog = dataCalalog;
            connectionString = ConnectionStringCreate();

        }
        public String ConnectionStringCreate()
        {
            if (databaseType == databasetype.MsSQL)
            {
                SqlConnectionStringBuilder CSB = new SqlConnectionStringBuilder();
                CSB.DataSource = Host;
                CSB.IntegratedSecurity = false;
                CSB.UserID = userid;
                CSB.Password = password;
                CSB.InitialCatalog = dataCatalog;                
                CSB.MultipleActiveResultSets = false;
                connectionString = CSB.ConnectionString;
                return CSB.ConnectionString;
            }
            else if (databaseType == databasetype.Firebird)
            {
                //FbConnectionStringBuilder CSB = new FbConnectionStringBuilder();
                //CSB.DataSource = host;
                //if (port != 3050)
                //{
                //    CSB.DataSource = host + "/" + port.ToString();
                //}
                //CSB.UserID = userid;
                //CSB.Password = password;
                //CSB.Database = dataCatalog;
                return "";
            }
            return "";
        }
        public String ConnectionStringCreate2()
        {
            if (databaseType == databasetype.MsSQL)
            {
                SqlConnectionStringBuilder CSB = new SqlConnectionStringBuilder();
                CSB.DataSource = Host;
                CSB.IntegratedSecurity = true;
                CSB.UserID = userid;
                CSB.Password = password;
                CSB.InitialCatalog = dataCatalog;
                CSB.MultipleActiveResultSets = false;
                connectionString = CSB.ConnectionString;
                return CSB.ConnectionString;
            }
            else if (databaseType == databasetype.Firebird)
            {
                //FbConnectionStringBuilder CSB = new FbConnectionStringBuilder();
                //CSB.DataSource = host;
                //if (port != 3050)
                //{
                //    CSB.DataSource = host + "/" + port.ToString();
                //}
                //CSB.UserID = userid;
                //CSB.Password = password;
                //CSB.Database = dataCatalog;
                return "";
            }
            return "";
        }
        private void ConnectionStringtoDetail()
        {
            if (connectionString != null && connectionString != "")
            {
                String[] Parcalar = connectionString.Split(';');
                if (Parcalar.Length > 0)
                {
                    foreach (String item in Parcalar)
                    {
                        //Data Source
                        int Baslangic = item.IndexOf("=") + 1;
                        if (item.ToUpper(new CultureInfo("en-US", false)).StartsWith("DATA SOURCE")) host = item.Substring(Baslangic, item.Length - Baslangic );
                        if (item.ToUpper(new CultureInfo("en-US", false)).StartsWith("USER ID")) userid = item.Substring(Baslangic, item.Length - Baslangic );
                        if (item.ToUpper(new CultureInfo("en-US", false)).StartsWith("PASSWORD")) password = item.Substring(Baslangic, item.Length - Baslangic );
                        if (item.ToUpper(new CultureInfo("en-US", false)).StartsWith("INITIAL CATALOG")) dataCatalog = item.Substring(Baslangic, item.Length - Baslangic);

                    }
                }
            }
        }
        public List<String> LocalServer()
            
        {
            
            List<String> L = new List<string>();
            RegistryKey localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            localKey = localKey.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
            if (localKey != null)
            {
                String[] I = (String[])localKey.GetValue("InstalledInstances");
                if (I !=null && I.Length > 0)
                {
                    foreach (String item in I)
                    {
                        L.Add(Environment.MachineName.ToString() + @"\" + item);
                    }
                }
                else L.Add(Environment.MachineName);
            }
            else L.Add(Environment.MachineName);

            return L;
        }
    }
}
