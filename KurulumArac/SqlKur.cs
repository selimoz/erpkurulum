﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Win32;
using System.Net;

namespace KurulumArac
{
    class SqlKur
    {
        Exception Hata = new Exception();
        String Path = "";
        public SqlKur(String AppPath)
        {
            Path = AppPath;
            if (!Directory.Exists(Path + @"\Data\SQL"))
            {
                Directory.CreateDirectory(Path + @"\Data\SQL");
            }
        }
        public Boolean SqlSetupIndir(Boolean tekrar = false)
        {
            bool Sonuc = false;
            String Link = "";
            if (File.Exists(Path + @"\data\sql\SQL2014Kur.exe"))
            {
                if (tekrar) File.Delete(Path + @"\data\sql\SQL2014Kur.exe"); else return true;
            }
            if (Environment.Is64BitOperatingSystem)
            {
                Link = "https://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/Express%2064BIT/SQLEXPR_x64_ENU.exe";
                DosyaIndir(Link, Path + @"\data\sql\SQL2014Kur.exe");
                Sonuc = true;
            }
            else
            {
                Link = "https://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/Express%2032BIT/SQLEXPR_x86_ENU.exe";
                DosyaIndir(Link, Path + @"\data\sql\SQL2014Kur.exe");
                Sonuc = true;
            }

            return Sonuc;
        }
        public Boolean DosyaIndir(String Link, String Dosya)
        {
            Boolean Sonuc = false;
            using (WebClient wCln = new WebClient())
            {
                try
                {
                    wCln.BaseAddress = Link;
                    wCln.Proxy = null;
                    wCln.Credentials = new NetworkCredential();
                    wCln.DownloadFile(Link, Dosya);
                    Sonuc = true;
                }
                catch (Exception Ht)
                {
                    Hata = Ht;
                    Sonuc = false;
                }

            }
            return Sonuc;
        }
        public String ParolaUret()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + ("ABCDEFGHİJKLMNOPQRSTUVWXYZ").ToLower().ToString() + ".,!*/-+%&/()=?:;[]{}$€₺@" ;
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 10)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result.ToString();

        }
    }
}
