﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Windows.Forms;

namespace KurulumArac
{
    class ErpAyarlar
    {
        private String ErpDosyaAdi = Application.StartupPath + "\\ERP12.exe.config";
        private String PosDosyaAdi = Application.StartupPath + "\\P_POS.exe.Config";
        public DatabaseInfo DBPcPosLocal;
        public DatabaseInfo DBPcPosServer;
        public DatabaseInfo DBServis;
        public Boolean PCPosDbDegisti = false;
        private XmlDocument icerik;
        private XmlDocument PosIcerik;
        public List<DataServer> LsServer = new List<DataServer>();
        public List<DatabaseInfo> DBList = new List<DatabaseInfo>();

        public XmlDocument Icerik { get => icerik; set => icerik = value; }

        public ErpAyarlar()
        {
            icerik = new XmlDocument();
            PosIcerik = new XmlDocument();
            try
            {
                icerik.Load(ErpDosyaAdi);
                PosIcerik.Load(PosDosyaAdi);
                Listele();
                ListelePos();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.ToString());
            }
        }

        private void ListelePos()
        {
            XmlNode NdLocal = PosIcerik.CreateElement("add");
            XmlNode NdServer = PosIcerik.CreateElement("add");
            foreach (XmlNode Nd in PosIcerik["configuration"]["connectionStrings"].ChildNodes)
            {
                if (Nd.Attributes["name"].Value.ToLower() == "local") NdLocal = Nd;
                if (Nd.Attributes["name"].Value.ToLower() == "server") NdServer = Nd;
            }
            if (DBPcPosLocal == null) DBPcPosLocal = new DatabaseInfo();
            if (DBPcPosServer == null) DBPcPosServer = new DatabaseInfo();
            DBPcPosLocal.ConnectionString = NdLocal.Attributes["connectionString"].Value;
            DBPcPosServer.ConnectionString = NdServer.Attributes["connectionString"].Value;
            DBPcPosLocal.Text = NdLocal.Attributes["name"].Value;
            DBPcPosServer.Text = NdServer.Attributes["name"].Value;
        }

        private void Listele()
        {
            String Lst = string.Empty;
            foreach (XmlNode Nd in icerik["configuration"]["connectionStrings"].ChildNodes)
            {
                DataBaseEkle(Nd);
                Lst += Nd.Attributes["name"].Value.ToString() + "\r\n";
            }
        }
        private void DataBaseEkle(XmlNode ND)
        {
            DatabaseInfo DB1 = new DatabaseInfo();
            DB1.Text = ND.Attributes["name"].Value.ToString();
            DB1.ConnectionString = ND.Attributes["connectionString"].Value.ToString();
            DBList.Add(DB1);

        }
        public Boolean ListedeServerVarmi(String serverName)
        {
            if (LsServer.Count > 0)
            {
                foreach (DataServer SRV in LsServer)
                {
                    if (SRV.ServerName.ToUpper() == serverName.Trim().ToUpper())
                    {
                        return true;
                    }
                }
                return false;
            }
            else return false;
        }
        public void DataBaseYukle(ref ListBox CMB)
        {
            if (DBList.Count > 0)
            {
                foreach (DatabaseInfo DB in DBList)
                {
                    CMB.Items.Add(DB.Text + " [ Sunucu : " + DB.Host + "], [Veritabanı : " + DB.DataCatalog + " ]");
                }
            }
        }
        public void ServerYukle(ref ListBox LS)
        {
            if (LsServer.Count > 0)
            {
                foreach (DataServer Dser in LsServer)
                {
                    LS.Items.Add(Dser.ServerName);
                }
            }
        }
        public void Yedekle()
        {
            String YDosya = ErpDosyaAdi.Trim().ToLower().Replace(".config", ".config." + DateTime.Now.ToString("yyMMddHHmm"));
            icerik.Save(YDosya);
        }
        public void PosYedekle(XmlDocument PosIcerik)
        {
            String YDosya = PosDosyaAdi.Trim().ToLower().Replace(".config", ".config." + DateTime.Now.ToString("yyMMddHHmm"));
            PosIcerik.Save(YDosya);
        }
        public void Kaydet()
        {
            Yedekle();
            XmlNode CS = icerik["configuration"]["connectionStrings"];
            CS.RemoveAll();
            foreach (DatabaseInfo DB in DBList)
            {
                DB.ConnectionStringCreate();
                XmlNode N1 = icerik.CreateElement("add");
                XmlAttribute A1 = icerik.CreateAttribute("name"); A1.Value = DB.Text;
                XmlAttribute A2 = icerik.CreateAttribute("connectionString"); A2.Value = DB.ConnectionString;
                N1.Attributes.Append(A1);
                N1.Attributes.Append(A2);
                CS.AppendChild(N1);
            }
            icerik.Save(ErpDosyaAdi);
            if (PCPosDbDegisti && File.Exists(PosDosyaAdi))
            {
                PosYedekle(PosIcerik);
                XmlNode NConfig = PosIcerik["configuration"]["connectionStrings"];
                XmlNode NdLocal = icerik.CreateElement("add");
                XmlNode NdServer = icerik.CreateElement("add");
                foreach (XmlNode Nd in NConfig.ChildNodes)
                {
                    if (Nd.Attributes["name"].Value.ToLower() == "local") NdLocal = Nd;
                    if (Nd.Attributes["name"].Value.ToLower() == "server") NdServer = Nd;
                }
                NConfig.RemoveAll();
                if (DBPcPosLocal != null) NdLocal.Attributes["connectionString"].Value = DBPcPosLocal.ConnectionString;
                if (DBPcPosServer != null) NdServer.Attributes["connectionString"].Value = DBPcPosServer.ConnectionString;
                NConfig.AppendChild(NdLocal);
                NConfig.AppendChild(NdServer);
                PosIcerik.Save(PosDosyaAdi);
            }
        }
    }
}
