﻿namespace KurulumArac
{
    partial class NMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Rapor Export");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Rapor Import");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Rapor İşlemleri", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Fiyat Kopyala");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Stok İşlemleri", new System.Windows.Forms.TreeNode[] {
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Sorgu Çalıştır");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Genel İşlemler", new System.Windows.Forms.TreeNode[] {
            treeNode6});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NMain));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnSQLKur = new System.Windows.Forms.Button();
            this.rbIstemci = new System.Windows.Forms.RadioButton();
            this.rbSunucu = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.groupPcPos = new System.Windows.Forms.GroupBox();
            this.lbPCPosServer = new System.Windows.Forms.Label();
            this.lbPcPosLocal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSunucuSil = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsVtAciklama = new System.Windows.Forms.ToolStripStatusLabel();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbServer = new System.Windows.Forms.ComboBox();
            this.btnSunucuOlustur = new System.Windows.Forms.Button();
            this.btnSunucuEkle = new System.Windows.Forms.Button();
            this.listCatalog = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkPassLabel = new System.Windows.Forms.CheckBox();
            this.btnDBTest = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.listSunucu = new System.Windows.Forms.ListBox();
            this.cmenuSunucu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuPCPosLocalSet = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPcPosServerSet = new System.Windows.Forms.ToolStripMenuItem();
            this.menuServisAktif = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuRaporKullanicisi = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAyarKaydet = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmenuSystem = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmBilgisayarAdDegis = new System.Windows.Forms.ToolStripMenuItem();
            this.cmYenidenBaslat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSaParolaDegistir = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.tsAciklama = new System.Windows.Forms.ToolStripStatusLabel();
            this.PBar = new System.Windows.Forms.ToolStripProgressBar();
            this.btnGuncelleme = new System.Windows.Forms.Button();
            this.btnServis = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGenel = new System.Windows.Forms.TabPage();
            this.tabEsZaman = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnFiyatEsitle = new System.Windows.Forms.Button();
            this.listEsZaman = new System.Windows.Forms.ListBox();
            this.btnStokEsitle = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridEvraklar = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.TXDurum = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDuzelt = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnListele = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbEZServer = new System.Windows.Forms.Label();
            this.lbEZLocal = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.lbServisDBI = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox5.SuspendLayout();
            this.groupPcPos.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.cmenuSunucu.SuspendLayout();
            this.cmenuSystem.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabGenel.SuspendLayout();
            this.tabEsZaman.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEvraklar)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.btnSQLKur);
            this.groupBox5.Controls.Add(this.rbIstemci);
            this.groupBox5.Controls.Add(this.rbSunucu);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.groupPcPos);
            this.groupBox5.Controls.Add(this.btnSunucuSil);
            this.groupBox5.Controls.Add(this.statusStrip1);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.cmbServer);
            this.groupBox5.Controls.Add(this.btnSunucuOlustur);
            this.groupBox5.Controls.Add(this.btnSunucuEkle);
            this.groupBox5.Controls.Add(this.listCatalog);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.chkPassLabel);
            this.groupBox5.Controls.Add(this.btnDBTest);
            this.groupBox5.Controls.Add(this.txtPassword);
            this.groupBox5.Controls.Add(this.txtUserName);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.listSunucu);
            this.groupBox5.Location = new System.Drawing.Point(9, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(995, 430);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Veritabanı Bağlantısı";
            // 
            // btnSQLKur
            // 
            this.btnSQLKur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSQLKur.Location = new System.Drawing.Point(82, 365);
            this.btnSQLKur.Name = "btnSQLKur";
            this.btnSQLKur.Size = new System.Drawing.Size(216, 25);
            this.btnSQLKur.TabIndex = 32;
            this.btnSQLKur.Text = "SQL Server 2014 Express";
            this.btnSQLKur.UseVisualStyleBackColor = true;
            this.btnSQLKur.Click += new System.EventHandler(this.btnSQLKur_Click);
            // 
            // rbIstemci
            // 
            this.rbIstemci.AutoSize = true;
            this.rbIstemci.Location = new System.Drawing.Point(190, 27);
            this.rbIstemci.Name = "rbIstemci";
            this.rbIstemci.Size = new System.Drawing.Size(95, 17);
            this.rbIstemci.TabIndex = 31;
            this.rbIstemci.Text = "İstemci (client)";
            this.rbIstemci.UseVisualStyleBackColor = true;
            this.rbIstemci.CheckedChanged += new System.EventHandler(this.rbIstemci_CheckedChanged);
            // 
            // rbSunucu
            // 
            this.rbSunucu.AutoSize = true;
            this.rbSunucu.Checked = true;
            this.rbSunucu.Location = new System.Drawing.Point(82, 27);
            this.rbSunucu.Name = "rbSunucu";
            this.rbSunucu.Size = new System.Drawing.Size(102, 17);
            this.rbSunucu.TabIndex = 30;
            this.rbSunucu.TabStop = true;
            this.rbSunucu.Text = "Sunucu (server)";
            this.rbSunucu.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label4.Location = new System.Drawing.Point(31, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Bağlantı";
            // 
            // groupPcPos
            // 
            this.groupPcPos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPcPos.Controls.Add(this.lbPCPosServer);
            this.groupPcPos.Controls.Add(this.lbPcPosLocal);
            this.groupPcPos.Controls.Add(this.label3);
            this.groupPcPos.Controls.Add(this.label2);
            this.groupPcPos.Location = new System.Drawing.Point(304, 325);
            this.groupPcPos.Name = "groupPcPos";
            this.groupPcPos.Size = new System.Drawing.Size(680, 65);
            this.groupPcPos.TabIndex = 28;
            this.groupPcPos.TabStop = false;
            this.groupPcPos.Text = "PC Pos Sunucuları";
            // 
            // lbPCPosServer
            // 
            this.lbPCPosServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPCPosServer.BackColor = System.Drawing.Color.Gainsboro;
            this.lbPCPosServer.Location = new System.Drawing.Point(57, 40);
            this.lbPCPosServer.Name = "lbPCPosServer";
            this.lbPCPosServer.Size = new System.Drawing.Size(617, 18);
            this.lbPCPosServer.TabIndex = 3;
            // 
            // lbPcPosLocal
            // 
            this.lbPcPosLocal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPcPosLocal.BackColor = System.Drawing.Color.Gainsboro;
            this.lbPcPosLocal.Location = new System.Drawing.Point(57, 17);
            this.lbPcPosLocal.Name = "lbPcPosLocal";
            this.lbPcPosLocal.Size = new System.Drawing.Size(617, 18);
            this.lbPcPosLocal.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Local";
            // 
            // btnSunucuSil
            // 
            this.btnSunucuSil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSunucuSil.Location = new System.Drawing.Point(905, 19);
            this.btnSunucuSil.Name = "btnSunucuSil";
            this.btnSunucuSil.Size = new System.Drawing.Size(79, 23);
            this.btnSunucuSil.TabIndex = 27;
            this.btnSunucuSil.Text = "Bağlantı Sil";
            this.btnSunucuSil.UseVisualStyleBackColor = true;
            this.btnSunucuSil.Click += new System.EventHandler(this.btnSunucuSil_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tsVtAciklama});
            this.statusStrip1.Location = new System.Drawing.Point(3, 403);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(989, 24);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(48, 19);
            this.toolStripStatusLabel1.Text = "Durum";
            // 
            // tsVtAciklama
            // 
            this.tsVtAciklama.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsVtAciklama.Name = "tsVtAciklama";
            this.tsVtAciklama.Size = new System.Drawing.Size(926, 19);
            this.tsVtAciklama.Spring = true;
            this.tsVtAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsVtAciklama.Click += new System.EventHandler(this.tsVtAciklama_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label8.Location = new System.Drawing.Point(316, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "ERP12 Veritabanı Bağlantıları";
            // 
            // cmbServer
            // 
            this.cmbServer.FormattingEnabled = true;
            this.cmbServer.Location = new System.Drawing.Point(82, 50);
            this.cmbServer.Name = "cmbServer";
            this.cmbServer.Size = new System.Drawing.Size(216, 21);
            this.cmbServer.TabIndex = 20;
            this.cmbServer.TextChanged += new System.EventHandler(this.cmbServer_TextChanged);
            // 
            // btnSunucuOlustur
            // 
            this.btnSunucuOlustur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSunucuOlustur.Location = new System.Drawing.Point(82, 334);
            this.btnSunucuOlustur.Name = "btnSunucuOlustur";
            this.btnSunucuOlustur.Size = new System.Drawing.Size(216, 25);
            this.btnSunucuOlustur.TabIndex = 24;
            this.btnSunucuOlustur.Text = "Yeni ERP Veritabanı Oluştur";
            this.btnSunucuOlustur.UseVisualStyleBackColor = true;
            this.btnSunucuOlustur.Click += new System.EventHandler(this.btnSunucuOlustur_Click);
            // 
            // btnSunucuEkle
            // 
            this.btnSunucuEkle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSunucuEkle.Enabled = false;
            this.btnSunucuEkle.Location = new System.Drawing.Point(82, 303);
            this.btnSunucuEkle.Name = "btnSunucuEkle";
            this.btnSunucuEkle.Size = new System.Drawing.Size(216, 25);
            this.btnSunucuEkle.TabIndex = 23;
            this.btnSunucuEkle.Text = "ERP Bağlantısı Ekle";
            this.btnSunucuEkle.UseVisualStyleBackColor = true;
            this.btnSunucuEkle.Click += new System.EventHandler(this.btnSunucuEkle_Click);
            // 
            // listCatalog
            // 
            this.listCatalog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listCatalog.FormattingEnabled = true;
            this.listCatalog.Location = new System.Drawing.Point(82, 127);
            this.listCatalog.Name = "listCatalog";
            this.listCatalog.Size = new System.Drawing.Size(216, 160);
            this.listCatalog.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label7.Location = new System.Drawing.Point(21, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Veritabanı";
            // 
            // chkPassLabel
            // 
            this.chkPassLabel.AutoSize = true;
            this.chkPassLabel.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPassLabel.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.chkPassLabel.Location = new System.Drawing.Point(20, 102);
            this.chkPassLabel.Name = "chkPassLabel";
            this.chkPassLabel.Size = new System.Drawing.Size(56, 17);
            this.chkPassLabel.TabIndex = 20;
            this.chkPassLabel.Text = "Parola";
            this.chkPassLabel.UseVisualStyleBackColor = true;
            this.chkPassLabel.CheckedChanged += new System.EventHandler(this.chkPassLabel_CheckedChanged);
            // 
            // btnDBTest
            // 
            this.btnDBTest.Location = new System.Drawing.Point(245, 98);
            this.btnDBTest.Name = "btnDBTest";
            this.btnDBTest.Size = new System.Drawing.Size(53, 23);
            this.btnDBTest.TabIndex = 6;
            this.btnDBTest.Text = "Sına";
            this.btnDBTest.UseVisualStyleBackColor = true;
            this.btnDBTest.Click += new System.EventHandler(this.btnDBTest_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(82, 100);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(157, 21);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(82, 75);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(216, 21);
            this.txtUserName.TabIndex = 4;
            this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label6.Location = new System.Drawing.Point(14, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Kullanıcı Adı";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label5.Location = new System.Drawing.Point(16, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Sunucu Adı";
            // 
            // listSunucu
            // 
            this.listSunucu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listSunucu.ContextMenuStrip = this.cmenuSunucu;
            this.listSunucu.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.listSunucu.FormattingEnabled = true;
            this.listSunucu.ItemHeight = 14;
            this.listSunucu.Location = new System.Drawing.Point(304, 50);
            this.listSunucu.Name = "listSunucu";
            this.listSunucu.Size = new System.Drawing.Size(680, 242);
            this.listSunucu.TabIndex = 0;
            // 
            // cmenuSunucu
            // 
            this.cmenuSunucu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmenuSunucu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuPCPosLocalSet,
            this.menuPcPosServerSet,
            this.menuServisAktif,
            this.toolStripSeparator1,
            this.menuRaporKullanicisi});
            this.cmenuSunucu.Name = "cmenuSunucu";
            this.cmenuSunucu.Size = new System.Drawing.Size(248, 98);
            // 
            // menuPCPosLocalSet
            // 
            this.menuPCPosLocalSet.Name = "menuPCPosLocalSet";
            this.menuPCPosLocalSet.Size = new System.Drawing.Size(247, 22);
            this.menuPCPosLocalSet.Text = "PC Pos için Lokal Sunucu Ayarla";
            this.menuPCPosLocalSet.Click += new System.EventHandler(this.menuPCPosLocalSet_Click);
            // 
            // menuPcPosServerSet
            // 
            this.menuPcPosServerSet.Name = "menuPcPosServerSet";
            this.menuPcPosServerSet.Size = new System.Drawing.Size(247, 22);
            this.menuPcPosServerSet.Text = "PC Pos için Server Sunucu Ayarla";
            this.menuPcPosServerSet.Click += new System.EventHandler(this.menuPcPosServerSet_Click);
            // 
            // menuServisAktif
            // 
            this.menuServisAktif.Name = "menuServisAktif";
            this.menuServisAktif.Size = new System.Drawing.Size(247, 22);
            this.menuServisAktif.Text = "Diğer İşlemler İçin Aktif Et";
            this.menuServisAktif.Click += new System.EventHandler(this.menuServisAktif_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(244, 6);
            // 
            // menuRaporKullanicisi
            // 
            this.menuRaporKullanicisi.Name = "menuRaporKullanicisi";
            this.menuRaporKullanicisi.Size = new System.Drawing.Size(247, 22);
            this.menuRaporKullanicisi.Text = "Rapor Kullanıcısı Oluştur";
            this.menuRaporKullanicisi.Click += new System.EventHandler(this.menuRaporKullanicisi_Click);
            // 
            // btnAyarKaydet
            // 
            this.btnAyarKaydet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAyarKaydet.Location = new System.Drawing.Point(859, 442);
            this.btnAyarKaydet.Name = "btnAyarKaydet";
            this.btnAyarKaydet.Size = new System.Drawing.Size(146, 37);
            this.btnAyarKaydet.TabIndex = 3;
            this.btnAyarKaydet.Text = "Ayarları Kaydet";
            this.btnAyarKaydet.UseVisualStyleBackColor = true;
            this.btnAyarKaydet.Click += new System.EventHandler(this.btnAyarKaydet_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ContextMenuStrip = this.cmenuSystem;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.Peru;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "ERP 12 Kurulum ve Yapılandırma";
            // 
            // cmenuSystem
            // 
            this.cmenuSystem.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmenuSystem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmBilgisayarAdDegis,
            this.cmYenidenBaslat,
            this.toolStripSeparator2,
            this.menuSaParolaDegistir});
            this.cmenuSystem.Name = "cmenuSystem";
            this.cmenuSystem.Size = new System.Drawing.Size(223, 76);
            // 
            // cmBilgisayarAdDegis
            // 
            this.cmBilgisayarAdDegis.Name = "cmBilgisayarAdDegis";
            this.cmBilgisayarAdDegis.Size = new System.Drawing.Size(222, 22);
            this.cmBilgisayarAdDegis.Text = "Bilgisayar Adını Değiştir";
            this.cmBilgisayarAdDegis.Click += new System.EventHandler(this.cmBilgisayarAdDegis_Click);
            // 
            // cmYenidenBaslat
            // 
            this.cmYenidenBaslat.Name = "cmYenidenBaslat";
            this.cmYenidenBaslat.Size = new System.Drawing.Size(222, 22);
            this.cmYenidenBaslat.Text = "Yeniden Başlat";
            this.cmYenidenBaslat.Click += new System.EventHandler(this.cmYenidenBaslat_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(219, 6);
            // 
            // menuSaParolaDegistir
            // 
            this.menuSaParolaDegistir.Name = "menuSaParolaDegistir";
            this.menuSaParolaDegistir.Size = new System.Drawing.Size(222, 22);
            this.menuSaParolaDegistir.Text = "SQL Kullanıcı Parola Değiştir";
            this.menuSaParolaDegistir.Click += new System.EventHandler(this.menuSaParolaDegistir_Click);
            // 
            // statusStrip2
            // 
            this.statusStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsAciklama,
            this.PBar});
            this.statusStrip2.Location = new System.Drawing.Point(0, 539);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1018, 22);
            this.statusStrip2.TabIndex = 5;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // tsAciklama
            // 
            this.tsAciklama.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.tsAciklama.Name = "tsAciklama";
            this.tsAciklama.Size = new System.Drawing.Size(901, 17);
            this.tsAciklama.Spring = true;
            // 
            // PBar
            // 
            this.PBar.Name = "PBar";
            this.PBar.Size = new System.Drawing.Size(100, 16);
            // 
            // btnGuncelleme
            // 
            this.btnGuncelleme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuncelleme.Location = new System.Drawing.Point(571, 442);
            this.btnGuncelleme.Name = "btnGuncelleme";
            this.btnGuncelleme.Size = new System.Drawing.Size(138, 37);
            this.btnGuncelleme.TabIndex = 6;
            this.btnGuncelleme.Text = "ERP 12 Güncelleme";
            this.btnGuncelleme.UseVisualStyleBackColor = true;
            this.btnGuncelleme.Click += new System.EventHandler(this.btnGuncelleme_Click);
            // 
            // btnServis
            // 
            this.btnServis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnServis.Location = new System.Drawing.Point(715, 442);
            this.btnServis.Name = "btnServis";
            this.btnServis.Size = new System.Drawing.Size(138, 37);
            this.btnServis.TabIndex = 7;
            this.btnServis.Text = "ERP 12 Servis";
            this.btnServis.UseVisualStyleBackColor = true;
            this.btnServis.Click += new System.EventHandler(this.btnServis_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabGenel);
            this.tabControl1.Controls.Add(this.tabEsZaman);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(60, 28);
            this.tabControl1.Location = new System.Drawing.Point(0, 18);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1018, 521);
            this.tabControl1.TabIndex = 8;
            // 
            // tabGenel
            // 
            this.tabGenel.Controls.Add(this.groupBox5);
            this.tabGenel.Controls.Add(this.btnGuncelleme);
            this.tabGenel.Controls.Add(this.btnAyarKaydet);
            this.tabGenel.Controls.Add(this.btnServis);
            this.tabGenel.Location = new System.Drawing.Point(4, 32);
            this.tabGenel.Name = "tabGenel";
            this.tabGenel.Padding = new System.Windows.Forms.Padding(3);
            this.tabGenel.Size = new System.Drawing.Size(1010, 485);
            this.tabGenel.TabIndex = 0;
            this.tabGenel.Text = "Veritabanı";
            this.tabGenel.UseVisualStyleBackColor = true;
            // 
            // tabEsZaman
            // 
            this.tabEsZaman.Controls.Add(this.tabControl2);
            this.tabEsZaman.Controls.Add(this.groupBox1);
            this.tabEsZaman.Location = new System.Drawing.Point(4, 32);
            this.tabEsZaman.Name = "tabEsZaman";
            this.tabEsZaman.Padding = new System.Windows.Forms.Padding(3);
            this.tabEsZaman.Size = new System.Drawing.Size(1010, 485);
            this.tabEsZaman.TabIndex = 1;
            this.tabEsZaman.Text = "Eşzamanlama";
            this.tabEsZaman.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 68);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1004, 414);
            this.tabControl2.TabIndex = 33;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnFiyatEsitle);
            this.tabPage1.Controls.Add(this.listEsZaman);
            this.tabPage1.Controls.Add(this.btnStokEsitle);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(996, 388);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Eşitmele İşlemleri";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnFiyatEsitle
            // 
            this.btnFiyatEsitle.Location = new System.Drawing.Point(6, 6);
            this.btnFiyatEsitle.Name = "btnFiyatEsitle";
            this.btnFiyatEsitle.Size = new System.Drawing.Size(225, 31);
            this.btnFiyatEsitle.TabIndex = 30;
            this.btnFiyatEsitle.Text = "Fiyat Eşitle";
            this.btnFiyatEsitle.UseVisualStyleBackColor = true;
            this.btnFiyatEsitle.Click += new System.EventHandler(this.btnFiyatEsitle_Click);
            // 
            // listEsZaman
            // 
            this.listEsZaman.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listEsZaman.FormattingEnabled = true;
            this.listEsZaman.Location = new System.Drawing.Point(237, 6);
            this.listEsZaman.Name = "listEsZaman";
            this.listEsZaman.Size = new System.Drawing.Size(753, 368);
            this.listEsZaman.TabIndex = 31;
            // 
            // btnStokEsitle
            // 
            this.btnStokEsitle.Location = new System.Drawing.Point(6, 43);
            this.btnStokEsitle.Name = "btnStokEsitle";
            this.btnStokEsitle.Size = new System.Drawing.Size(225, 31);
            this.btnStokEsitle.TabIndex = 32;
            this.btnStokEsitle.Text = "Stok Eşitle";
            this.btnStokEsitle.UseVisualStyleBackColor = true;
            this.btnStokEsitle.Visible = false;
            this.btnStokEsitle.Click += new System.EventHandler(this.btnStokEsitle_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridEvraklar);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(996, 388);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Gönderilemeyenler";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridEvraklar
            // 
            this.gridEvraklar.AllowUserToAddRows = false;
            this.gridEvraklar.AllowUserToDeleteRows = false;
            this.gridEvraklar.AllowUserToOrderColumns = true;
            this.gridEvraklar.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridEvraklar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridEvraklar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEvraklar.Location = new System.Drawing.Point(3, 31);
            this.gridEvraklar.Name = "gridEvraklar";
            this.gridEvraklar.ReadOnly = true;
            this.gridEvraklar.RowHeadersVisible = false;
            this.gridEvraklar.Size = new System.Drawing.Size(990, 209);
            this.gridEvraklar.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TXDurum);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 240);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(990, 145);
            this.panel2.TabIndex = 2;
            // 
            // TXDurum
            // 
            this.TXDurum.BackColor = System.Drawing.Color.Gainsboro;
            this.TXDurum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TXDurum.Location = new System.Drawing.Point(0, 0);
            this.TXDurum.Multiline = true;
            this.TXDurum.Name = "TXDurum";
            this.TXDurum.Size = new System.Drawing.Size(990, 145);
            this.TXDurum.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDuzelt);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.btnListele);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(990, 28);
            this.panel1.TabIndex = 1;
            // 
            // btnDuzelt
            // 
            this.btnDuzelt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDuzelt.Enabled = false;
            this.btnDuzelt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDuzelt.Location = new System.Drawing.Point(904, 2);
            this.btnDuzelt.Name = "btnDuzelt";
            this.btnDuzelt.Size = new System.Drawing.Size(83, 23);
            this.btnDuzelt.TabIndex = 2;
            this.btnDuzelt.Text = "Düzelt";
            this.btnDuzelt.UseVisualStyleBackColor = true;
            this.btnDuzelt.Click += new System.EventHandler(this.btnDuzelt_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Sienna;
            this.label9.Location = new System.Drawing.Point(5, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(168, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Merkeze Gönderilemeyen Belgeler";
            // 
            // btnListele
            // 
            this.btnListele.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnListele.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnListele.Location = new System.Drawing.Point(833, 2);
            this.btnListele.Name = "btnListele";
            this.btnListele.Size = new System.Drawing.Size(65, 23);
            this.btnListele.TabIndex = 0;
            this.btnListele.Text = "Listele";
            this.btnListele.UseVisualStyleBackColor = true;
            this.btnListele.Click += new System.EventHandler(this.btnListele_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbEZServer);
            this.groupBox1.Controls.Add(this.lbEZLocal);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1004, 65);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PC Pos Sunucuları";
            // 
            // lbEZServer
            // 
            this.lbEZServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEZServer.BackColor = System.Drawing.Color.Gainsboro;
            this.lbEZServer.Location = new System.Drawing.Point(57, 40);
            this.lbEZServer.Name = "lbEZServer";
            this.lbEZServer.Size = new System.Drawing.Size(941, 18);
            this.lbEZServer.TabIndex = 3;
            // 
            // lbEZLocal
            // 
            this.lbEZLocal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEZLocal.BackColor = System.Drawing.Color.Gainsboro;
            this.lbEZLocal.Location = new System.Drawing.Point(57, 17);
            this.lbEZLocal.Name = "lbEZLocal";
            this.lbEZLocal.Size = new System.Drawing.Size(941, 18);
            this.lbEZLocal.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Server";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Local";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 32);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1010, 485);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Diğer İşlemler";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(3, 60);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            this.splitContainer1.Size = new System.Drawing.Size(1004, 422);
            this.splitContainer1.SplitterDistance = 200;
            this.splitContainer1.TabIndex = 0;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "NdRaporExport";
            treeNode1.Text = "Rapor Export";
            treeNode2.Name = "NdRaporImport";
            treeNode2.Text = "Rapor Import";
            treeNode3.Name = "NdRapor";
            treeNode3.Text = "Rapor İşlemleri";
            treeNode4.Name = "NdFiyatKopyala";
            treeNode4.Text = "Fiyat Kopyala";
            treeNode5.Name = "NdStokIslem";
            treeNode5.Text = "Stok İşlemleri";
            treeNode6.Name = "NdSorgu";
            treeNode6.Text = "Sorgu Çalıştır";
            treeNode7.Name = "NDGenel";
            treeNode7.Text = "Genel İşlemler";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode5,
            treeNode7});
            this.treeView1.Size = new System.Drawing.Size(200, 422);
            this.treeView1.TabIndex = 0;
            this.treeView1.DoubleClick += new System.EventHandler(this.treeView1_DoubleClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.lbServisDBI);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1004, 57);
            this.panel3.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(8, 25);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(991, 25);
            this.label13.TabIndex = 0;
            this.label13.Text = "Lütfen bu işlemlerden önce YEDEK almayı UNUTMAYINIZ..!";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbServisDBI
            // 
            this.lbServisDBI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbServisDBI.BackColor = System.Drawing.Color.Gainsboro;
            this.lbServisDBI.Location = new System.Drawing.Point(97, 4);
            this.lbServisDBI.Name = "lbServisDBI";
            this.lbServisDBI.Size = new System.Drawing.Size(902, 18);
            this.lbServisDBI.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.DimGray;
            this.label10.Location = new System.Drawing.Point(5, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Erp12 Veritabanı";
            // 
            // NMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 561);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ERP12 Kurulum ve Yapılandırma Aracı";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NMain_FormClosing);
            this.Load += new System.EventHandler(this.NMain_Load);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupPcPos.ResumeLayout(false);
            this.groupPcPos.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.cmenuSunucu.ResumeLayout(false);
            this.cmenuSystem.ResumeLayout(false);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabGenel.ResumeLayout(false);
            this.tabEsZaman.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEvraklar)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listSunucu;
        private System.Windows.Forms.Button btnDBTest;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkPassLabel;
        private System.Windows.Forms.ListBox listCatalog;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSunucuEkle;
        private System.Windows.Forms.Button btnSunucuOlustur;
        private System.Windows.Forms.ComboBox cmbServer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tsVtAciklama;
        private System.Windows.Forms.Button btnSunucuSil;
        private System.Windows.Forms.Button btnAyarKaydet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip cmenuSunucu;
        private System.Windows.Forms.ToolStripMenuItem menuPCPosLocalSet;
        private System.Windows.Forms.ToolStripMenuItem menuPcPosServerSet;
        private System.Windows.Forms.GroupBox groupPcPos;
        private System.Windows.Forms.Label lbPCPosServer;
        private System.Windows.Forms.Label lbPcPosLocal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbSunucu;
        private System.Windows.Forms.RadioButton rbIstemci;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSQLKur;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.Button btnGuncelleme;
        private System.Windows.Forms.Button btnServis;
        private System.Windows.Forms.ToolStripStatusLabel tsAciklama;
        private System.Windows.Forms.ContextMenuStrip cmenuSystem;
        private System.Windows.Forms.ToolStripMenuItem cmBilgisayarAdDegis;
        private System.Windows.Forms.ToolStripMenuItem cmYenidenBaslat;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabGenel;
        private System.Windows.Forms.TabPage tabEsZaman;
        private System.Windows.Forms.Button btnFiyatEsitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbEZServer;
        private System.Windows.Forms.Label lbEZLocal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListBox listEsZaman;
        private System.Windows.Forms.ToolStripProgressBar PBar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuRaporKullanicisi;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuSaParolaDegistir;
        private System.Windows.Forms.Button btnStokEsitle;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView gridEvraklar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnListele;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TXDurum;
        private System.Windows.Forms.Button btnDuzelt;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbServisDBI;
        private System.Windows.Forms.ToolStripMenuItem menuServisAktif;
        private System.Windows.Forms.Label label13;
    }
}

