﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KurulumArac
{
    class EsZamanlama
    {
        ListBox Ls;
        private DatabaseInfo EzLocal = new DatabaseInfo();
        private DatabaseInfo EzServer = new DatabaseInfo();
        public EsZamanlama(String csLocal, String csServer)
        {
            Ls = null;

            EzLocal.ConnectionString = csLocal;
            EzServer.ConnectionString = csServer;
        }

        public EsZamanlama(String csLocal, String csServer, ListBox lb)
        {
            Ls = lb;
            if (lb == null) lb = new ListBox();
            EzLocal.ConnectionString = csLocal;
            EzServer.ConnectionString = csServer;
        }
        public void TabloGuncelle(String TabloAd, ref ToolStripProgressBar PBar)
        {
            String[] tipNumeric = { "Int32", "Int64", "Int16", "Integer" };
            String[] tipMetin = { "Char", "String", "Guid" };
            String[] tipRakam = { "Double", "Decimal" };
            String[] tipTarih = { "DateTime", "TimeSpan" };

            String YTabloAd = TabloAd + "_" + DateTime.Now.ToString("yyMMddHHmm");
            if (!DataLayer.BaglantiTest(EzLocal))
            {
                Ls.Items.Add("Yerel Veritabanı Bağlantısı Sağlanamadı..!");
                return;
            }
            else Ls.Items.Add("Yerel Veritabanı Bağlantısı Sağlandı..!");
            if (!DataLayer.BaglantiTest(EzServer))
            {
                Ls.Items.Add("Sunucu Veritabanı Bağlantısı Sağlanamadı..!");
            }
            else Ls.Items.Add("Sunucu Veritabanı Bağlantısı Sağlandı..!");

            Int32 SobjeId = DataLayer.SQLInt32("select object_id From sys.tables where name='" + TabloAd + "'", -1, EzServer);
            if (SobjeId == -1)
            {
                Ls.Items.Add("Sunucu Veritabanında " + TabloAd + " tablosu bulunamadı..!");
                return;
            }
            Int32 LobjeId = DataLayer.SQLInt32("select object_id From sys.tables where name='" + TabloAd + "'", -1, EzLocal);
            if (LobjeId == -1)
            {
                Ls.Items.Add("Yerel Veritabanında " + TabloAd + " tablosu bulunamadı..!");
                return;
            }
            Application.DoEvents();
            DataTable tb0 = new DataTable();
            //DataLayer.TabloDoldur(ref tb0, "EXEC INSERT_SCRIPT_HAZIRLA 'SELECT * FROM " + TabloAd + "',500", EzServer);
            DataLayer.TabloDoldur(ref tb0, "SELECT * FROM " + TabloAd, EzServer);
            if (tb0 != null && tb0.Rows.Count > 0)
            {

                Ls.Items.Add(TabloAd + " > " + YTabloAd + " Kayıt Sayısı : " + tb0.Rows.Count);
                if (!DataLayer.SQLCalistir("SELECT * INTO " + YTabloAd + " FROM " + TabloAd + " WHERE 1=0", EzLocal))
                {
                    Ls.Items.Add("Yerel Veritabanında " + YTabloAd + " tablosu oluşturulamadı..!");
                    return;
                }
                DataTable tbYerli = new DataTable();
                DataLayer.TabloDoldur(ref tbYerli, "SELECT * FROM " + YTabloAd, EzLocal);
                StringBuilder Sb = new StringBuilder();
                Int32 Sira = 0;
                PBar.Maximum = tb0.Rows.Count;
                PBar.Minimum = 0;
                PBar.Value = 0;
                String Kolon = "";
                foreach (DataRow rW in tb0.Rows)
                {
                    PBar.Value++;
                    Sira++;
                    //Decimal FiyDec; if (!Decimal.TryParse(rW["FIYAT"].ToString(), out FiyDec)) FiyDec = 0;
                    //String Kayit = "(" + rW["ID"].ToString() + "," + FiyDec.ToString("#0.#00000").Replace(",", ".") + "),";
                    String Kayit = "(";
                    Kolon = "(";
                    foreach (DataColumn Cln in tbYerli.Columns)
                    {
                        if (!Kolon.EndsWith("(")) Kolon += ",";
                        Kolon += Cln.ColumnName;
                        if (rW[Cln.ColumnName] == null)
                        {
                            if (!Kayit.EndsWith("(")) Kayit += ",";
                            Kayit += "null";
                            continue;
                        }
                        if (tipNumeric.Contains(Cln.DataType.Name))
                        {
                            Int64 Sayi; if (!Int64.TryParse(rW[Cln.ColumnName].ToString(), out Sayi)) Sayi = 0;
                            if (!Kayit.EndsWith("(")) Kayit += ",";
                            Kayit += Sayi.ToString("#0").Replace(",", ".");
                        }
                        if (tipMetin.Contains(Cln.DataType.Name))
                        {
                            if (!Kayit.EndsWith("(")) Kayit += ",";
                            Kayit += "'" + rW[Cln.ColumnName].ToString().Replace("'", "''") + "'";
                        }
                        if (tipTarih.Contains(Cln.DataType.Name))
                        {
                            DateTime Trh; if (!DateTime.TryParse(rW[Cln.ColumnName].ToString(), out Trh)) Trh = DateTime.Now;
                            if (!Kayit.EndsWith("(")) Kayit += ",";
                            Kayit += "'" + Trh.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                        }
                        if (tipRakam.Contains(Cln.DataType.Name))
                        {
                            Decimal valDec; if (!Decimal.TryParse(rW[Cln.ColumnName].ToString(), out valDec)) valDec = 0;
                            if (!Kayit.EndsWith("(")) Kayit += ",";
                            Kayit += valDec.ToString("#0.#000000").Replace(",", ".");
                        }
                        if (Cln.DataType.Name == "Boolean")
                        {
                            Boolean Sayi; if (!Boolean.TryParse(rW[Cln.ColumnName].ToString(), out Sayi)) Sayi = false;
                            if (!Kayit.EndsWith("(")) Kayit += ",";
                            if (Sayi) Kayit += "1"; else Kayit += "0";
                        }
                    }

                    Kayit += "),";
                    Sb.Append(Kayit);
                    if ((Sira % 500) == 0)
                    {
                        string cKomut = Sb.ToString();
                        if (cKomut.Trim().EndsWith(",")) cKomut = cKomut.Trim().Substring(0, cKomut.Trim().Length - 1);
                        if (DataLayer.SQLCalistir("INSERT INTO " + YTabloAd + " " + Kolon + ") VALUES " + cKomut, EzLocal))
                        {

                        }
                        Sb.Clear();
                        Application.DoEvents();
                    }
                }
                if (Sb.Length > 0)
                {
                    string cKomut = Sb.ToString();
                    if (cKomut.Trim().EndsWith(",")) cKomut = cKomut.Trim().Substring(0, cKomut.Trim().Length - 1);

                    if (DataLayer.SQLCalistir("INSERT INTO " + YTabloAd + " " + Kolon + ") VALUES " + cKomut, EzLocal))
                    {

                    }
                    Sb.Clear();
                }
                //if (DataLayer.SQLCalistir("UPDATE L SET L.FIYAT=S.FIYAT FROM " + TabloAd + " L INNER JOIN " + YTabloAd + " S ON S.ID=L.ID WHERE L.FIYAT<>S.FIYAT", EzLocal))
                //{
                //    Ls.Items.Add("İşlem Tamamlandı..!");
                //}

                String UKomut = "UPDATE L SET ";
                foreach (DataColumn cln in tbYerli.Columns)
                {
                    if (cln.ColumnName != "ID")
                    {
                        if (!UKomut.EndsWith("SET ")) UKomut += ",";
                        UKomut += " L." + cln.ColumnName + " = S." + cln.ColumnName;
                    }
                }
                UKomut += " FROM " + TabloAd + " L INNER JOIN " + YTabloAd + " S ON S.ID=L.ID";
                if (DataLayer.SQLCalistir(UKomut, EzLocal))
                {
                    Ls.Items.Add(TabloAd + " Güncelleme İşlem Tamamlandı..!");
                }

                String IKomut = "INSERT INTO " + TabloAd + " SELECT L.* FROM " + YTabloAd + " L LEFT JOIN " + TabloAd + " S ON S.ID=L.ID WHERE S.ID IS NULL";
                if (DataLayer.SQLCalistir(IKomut, EzLocal))
                {
                    Ls.Items.Add(TabloAd + " Ekleme İşlem Tamamlandı..!");
                }

                if (DataLayer.SQLCalistir("DROP TABLE " + YTabloAd, EzLocal))
                {
                    Ls.Items.Add(YTabloAd + " Temizlendi..!");
                }

                PBar.Value = 0;
            }
            else Ls.Items.Add("Server Veritabanında " + TabloAd + " tablosunda kayıt bulunamadı..!");
            Ls.Items.Add("Stok Güncelleme TAMAMLANDI...!");

        }
        public void problemliEvrakListe(ref DataGridView GRD)
        {
            String BKomut = @"SELECT 
PG.ID,L.AD LOKASYON,PG.TARIH,PG.BELGE_NUMARASI
,C.AD CARI_AD,PG.CARI_ADRES
,PG.MASA,MASA.AD MASA_AD
,PG.PERSONEL,P.AD PERSONEL_AD
,PG.PC_ID,PC.KOD PC_KOD,PC.AD PC_AD
,PG.LOKASYON LOKASYON_ID,PG.CARI
FROM POS_GECICI PG
LEFT JOIN LOKASYON L ON PG.LOKASYON=L.ID 
LEFT JOIN CARI C ON C.ID=PG.CARI
LEFT JOIN POS_MASA MASA ON MASA.ID=PG.MASA
LEFT JOIN PC_AD PC ON PC.ID=PG.PC_ID
LEFT JOIN CARI P ON P.ID=PG.PERSONEL
WHERE KAPANDI=1 AND ANA_DATAYA_ISLENDI=0";


            DataTable tbEvraklar = new DataTable();
            DataLayer.TabloDoldur(ref tbEvraklar, BKomut, EzLocal);
            GRD.DataSource = tbEvraklar;
        }
        public void problemliEvrakDuzelt(ref DataTable TB, ref TextBox TX)
        {
            // PC AD Kontrol
            DataTable tbPcAd = TB.DefaultView.ToTable(true, "PC_ID", "PC_KOD", "PC_AD");
            if (TX != null) TX.Text = "PC Ad & Id Kontrol\r\n";
            int hatasay = 0;
            foreach (DataRow rwPC in tbPcAd.Rows)
            {

                Int32 PcID = DataLayer.SQLInt32("SELECT TOP 1 ID FROM PC_AD WHERE KOD='" + rwPC["PC_KOD"].ToString() + "' AND ID=" + rwPC["PC_ID"].ToString(), -1, EzServer);
                if (PcID == -1)
                {
                    hatasay++;
                    PcID = DataLayer.SQLInt32("SELECT TOP 1 ID FROM PC_AD WHERE KOD='" + rwPC["PC_KOD"].ToString() + "'", -1, EzServer);
                    DataLayer.SQLCalistir("INSERT INTO PC_AD (ID,AD,KOD) VALUES (" + PcID.ToString() + ",'" + rwPC["PC_AD"].ToString() + "','" + rwPC["PC_KOD"] + "')", EzLocal);
                    DataLayer.SQLCalistir("UPDATE POS_GECICI SET PC_ID=" + PcID.ToString() + " WHERE PC_ID=" + rwPC["PC_ID"].ToString(), EzLocal);
                    DataLayer.SQLCalistir("DELETE FROM PC_AD WHERE ID=" + rwPC["PC_ID"].ToString(), EzLocal);
                    TX.Text += "PC ID Düzeltildi [" + rwPC["PC_AD"].ToString() + "]...!\r\n";
                }

            }
            if (hatasay == 0) TX.Text += "PC ID Problemi Bulunamadı...!";
            // Cari Kart Kontrolü
            DataTable tbCari = TB.DefaultView.ToTable(true, "CARI", "CARI_AD");
            if (TX != null) TX.Text += "Cari Kart Kontrol\r\n";
            hatasay = 0;
            foreach (DataRow rCari in tbCari.Rows)
            {
                Int32 cariId = DataLayer.SQLInt32("SELECT TOP 1 ID FROM CARI WHERE ID=" + rCari["CARI"].ToString(), -1, EzServer);
                if (cariId == -1)
                {
                    hatasay++;
                    if (rCari["CARI_AD"].Equals(DBNull.Value)) rCari["CARI_AD"] = rCari["CARI"].ToString() + " Id li Cari";
                    Int32 xCariId; if (!Int32.TryParse(rCari["CARI"].ToString(), out xCariId)) xCariId = 0;
                    if (xCariId > 0) cariEkle(xCariId, rCari["CARI_AD"].ToString());
                    TX.Text += "Server'a Cari Eklendi \r\n";
                }
            }
            if (hatasay == 0) TX.Text += "Cari Problemi Bulunamadı...!";
            // Personel Kontrolü
            DataTable tbPersonel = TB.DefaultView.ToTable(true, "PERSONEL", "PERSONEL_AD");
            if (TX != null) TX.Text += "Personel Kart Kontrol\r\n";
            hatasay = 0;
            foreach (DataRow rCari in tbPersonel.Rows)
            {
                Int32 cariId = DataLayer.SQLInt32("SELECT TOP 1 ID FROM CARI WHERE ID=" + rCari["PERSONEL"].ToString(), -1, EzServer);
                if (cariId == -1)
                {
                    hatasay++;
                    if (rCari["PERSONEL_AD"].Equals(DBNull.Value)) rCari["PERSONEL_AD"] = rCari["PERSONEL"].ToString() + " Id li Personel";
                    Int32 xCariId; if (!Int32.TryParse(rCari["PERSONEL"].ToString(), out xCariId)) xCariId = 0;
                    if (xCariId > 0) cariEkle(xCariId, rCari["PERSONEL_AD"].ToString(), 2);
                    TX.Text += "Server'a Personel Eklendi \r\n";
                }
            }
            if (hatasay == 0) TX.Text += "Personel Problemi Bulunamadı...!\r\n";
            // Masa Kontrolü
            DataTable tbMasa = TB.DefaultView.ToTable(true, "MASA", "MASA_AD");
            if (TX != null) TX.Text += "Masa Kart Kontrol\r\n";
            hatasay++;
            foreach (DataRow rMasa in tbMasa.Rows)
            {
                Int32 masaId = DataLayer.SQLInt32("SELECT TOP 1 ID FROM POS_MASA WHERE ID=" + rMasa["MASA"].ToString(), -1, EzServer);
                if (masaId == -1)
                {
                    hatasay++;
                    if (rMasa["MASA_AD"].Equals(DBNull.Value)) rMasa["MASA_AD"] = rMasa["MASA"].ToString() + " ID Masa";
                    Int32 xCariId; if (!Int32.TryParse(rMasa["MASA"].ToString(), out xCariId)) xCariId = 0;
                    if (xCariId > 0) masaEkle(xCariId, rMasa["MASA_AD"].ToString());
                    TX.Text += "Server'a Masa Eklendi \r\n";
                }
            }
            if (hatasay == 0) TX.Text += "Masa Problemi Bulunamadı...!";
            // DETAY VERİ KONTROL
            if (TX != null) TX.Text += "Stok ve Birim Kontrol\r\n";
            String DKomut = @"SELECT 
DISTINCT D.STOK,D.BARKOD,D.STOK_BIRIM,D.CARPAN,D.GARSON,S.KOD,S.AD
FROM POS_GECICI_DETAY D
INNER JOIN POS_GECICI G ON G.ID=D.POS_GECICI
INNER JOIN STOK S ON S.ID=D.STOK
WHERE G.KAPANDI=1 AND G.ANA_DATAYA_ISLENDI=0";
            DataTable tbDetay = new DataTable();
            DataLayer.TabloDoldur(ref tbDetay, DKomut, EzLocal);
            hatasay = 0;
            foreach (DataRow rDetay in tbDetay.Rows)
            {

                String barkod = rDetay["BARKOD"].ToString();
                String ad = rDetay["AD"].ToString();
                Int32 stok; if (!Int32.TryParse(rDetay["STOK"].ToString(), out stok)) stok = 0;
                Int32 stokBirim; if (!Int32.TryParse(rDetay["STOK_BIRIM"].ToString(), out stokBirim)) stokBirim = 0;
                Int32 garson; if (!Int32.TryParse(rDetay["GARSON"].ToString(), out garson)) garson = 0;
                Decimal carpan; if (!Decimal.TryParse(rDetay["CARPAN"].ToString(), out carpan)) carpan = 1;

                Int32 barkodId = DataLayer.SQLInt32("SELECT TOP 1 ID FROM STOK_BARKOD WHERE BARKOD='" + barkod + "'", -1, EzServer);
                Int32 stokId = DataLayer.SQLInt32("SELECT TOP 1 ID FROM STOK WHERE ID=" + stok, -1, EzServer);
                Int32 stokBirimId = DataLayer.SQLInt32("SELECT TOP 1 ID FROM STOK_STOK_BIRIM WHERE STOK=" + stokId + " AND STOK_BIRIM=" + stokBirim, -1, EzServer);
                Int32 barkodId2 = DataLayer.SQLInt32("SELECT TOP 1 ID FROM STOK_BARKOD WHERE STOK_STOK_BIRIM=" + stokBirimId + " BARKOD='" + barkod + "'", -1, EzServer);
                // Barkod Bulunamadı.....!
                if (barkodId == -1)
                {
                    if (stokId == -1)
                    {
                        stokEkle(stok, ad, barkod, stokBirim, EzServer);
                    }
                    else
                    {
                        if (stokId > 0 && stokBirimId == -1)
                        {
                            Int32 SSBId = DataLayer.SQLSequences("STOK_STOK_BIRIM", EzServer);
                            String BKomut = "INSERT INTO STOK_STOK_BIRIM (ID,STOK,STOK_BIRIM,CARPAN,VARSAYILAN,AGIRLIK,HACIM,EN,BOY,YUKSEKLIK) VALUES (";
                            BKomut += SSBId.ToString() + "," + stokId.ToString() + "," + stokBirim.ToString() + ",1,1,0,0,0,0,0)";
                            if (DataLayer.SQLCalistir(BKomut, EzServer))
                            {
                                stokBirimId = SSBId;
                            }
                        }
                        if (stokId > 0 && stokBirimId > 0)
                        {
                            barkodId = DataLayer.SQLSequences("STOK_BARKOD", EzServer);
                            String CKomut = "INSERT INTO STOK_BARKOD (ID,STOK_STOK_BIRIM,BARKOD,KISAYOL,AKTIF) VALUES (";
                            CKomut += barkodId.ToString() + "," + stokBirimId + ",'" + barkod + "',1,1)";
                            if (DataLayer.SQLCalistir(CKomut, EzServer))
                            {
                                continue;
                            }
                        }
                    }
                }
                if (barkodId > 0 && barkodId == barkodId2)
                {
                    DataTable tbstokBilgi = new DataTable();
                    DataLayer.TabloDoldur(ref tbstokBilgi, "SELECT TOP 1 SSB.STOK,SSB.STOK_BIRIM,SSB.CARPAN,B.BARKOD FROM STOK_STOK_BIRIM SSB LEFT JOIN STOK_BARKOD B ON B.STOK_STOK_BIRIM= SSB.ID WHERE B.BARKOD='" + barkod + "'", EzServer);
                    if (tbstokBilgi != null && tbstokBilgi.Rows.Count > 0)
                    {
                        DataRow RW = tbstokBilgi.Rows[0];
                        Int32 yStokId; if (!Int32.TryParse(RW["STOK"].ToString(), out yStokId)) yStokId = 0;
                        Int32 yStokBirimId; if (!Int32.TryParse(RW["STOK_BIRIM"].ToString(), out yStokBirimId)) yStokBirimId = 0;
                        Decimal yCarpan; if (!Decimal.TryParse(RW["CARPAN"].ToString(), out yCarpan)) yCarpan = 0;

                        if (stok != yStokId)
                        {
                            stokEkle(yStokId, ad, barkod, yStokBirimId, EzLocal);
                            String BKomut = "UPDATE POS_GECICI_DETAY SET STOK=" + yStokId + ", STOK_BIRIM=" + yStokBirimId + " WHERE STOK=" + stokId + " AND STOK_BIRIM=" + stokBirimId;
                            if (DataLayer.SQLCalistir(BKomut, EzLocal))
                            {

                            }
                        }
                        else
                        {
                            if (stokBirim != yStokBirimId)
                            {
                                String BKomut = "UPDATE STOK_STOK_BIRIM SET STOK_BIRIM=" + yStokBirimId + ",CARPAN=" + yCarpan.ToString("#0.#00000").Replace(",", ".") + " WHERE STOK=" + stokId + " AND STOK_BIRIM=" + stokBirimId;
                                if (DataLayer.SQLCalistir(BKomut, EzLocal))
                                {
                                    BKomut = "UPDATE POS_GECICI_DETAY SET STOK_BIRIM=" + yStokBirimId + " WHERE STOK=" + stokId + " AND STOK_BIRIM=" + stokBirimId;
                                    if (DataLayer.SQLCalistir(BKomut, EzLocal))
                                    {

                                    }
                                }
                            }
                        }
                    }
                }

                if (barkodId2 == -1)
                {

                    DataTable tbGStok = new DataTable();
                    DataLayer.TabloDoldur(ref tbGStok, "SELECT SSB.STOK_BIRIM,SSB.STOK FROM STOK_STOK_BIRIM SSB LEFT JOIN STOK_BARKOD B ON B.STOK_STOK_BIRIM=SSB.ID WHERE B.BARKOD='" + barkod + "'", EzServer);
                    if (tbGStok != null && tbGStok.Rows.Count > 0)
                    {
                        Int32 hStokId; if (!Int32.TryParse(tbGStok.Rows[0]["STOK"].ToString(), out hStokId)) hStokId = 0;
                        Int32 hStokBirimId; if (!Int32.TryParse(tbGStok.Rows[0]["STOK_BIRIM"].ToString(), out hStokBirimId)) hStokBirimId = 0;
                        if (hStokId > 0 && hStokBirimId > 0)
                        {
                            if (!DataLayer.SQLCalistir("UPDATE POS_GECICI_DETAY SET STOK=" + hStokId + " , STOK_BIRIM=" + hStokBirimId + " WHERE STOK=" + stok, EzLocal))
                            {
                                MessageBox.Show(DataLayer.HataMesaj.ToString());
                            }
                        }
                    }
                }

            }
            if (TX != null) TX.Text += "TÜM KONTROLLER TAMAMLANDI.\r\nListedeki fişler ilk veri gönderiminde Merkez server'a gönderilecektir.\r\nAksi durumda ERP12 bayi veya destek ekibi ile görüşünüz. \r\n";
        }

        private Boolean masaEkle(Int32 Id, String Ad)
        {
            DataTable tbMasa = new DataTable();
            DataLayer.TabloDoldur(ref tbMasa, "SELECT TOP 1 * FROM POS_MASA WHERE ID>0 AND USTID>0", EzServer);
            String IKomut = "INSERT INTO POS_MASA (";
            String IKolon = "";
            if (tbMasa != null && tbMasa.Rows.Count > 0)
            {
                foreach (DataColumn Cln in tbMasa.Columns)
                {
                    IKomut += Cln.ColumnName.ToString() + ",";
                    if (Cln.ColumnName.Equals("ID"))
                    {
                        IKolon += Id.ToString() + ",";
                    }
                    else
                    if (Cln.ColumnName.Equals("AD"))
                    {
                        IKolon += "'" + Ad.ToString() + "',";
                    }
                    else
                        IKolon += Cln.ColumnName.ToString() + ",";
                }
                IKomut = IKomut.TrimEnd(',') + ") SELECT " + IKolon.TrimEnd(',') + " FROM POS_MASA WHERE ID=" + tbMasa.Rows[0]["ID"].ToString();
                if (DataLayer.SQLCalistir(IKomut, EzServer))
                {
                    return true;
                }
                else return false;
            }
            return false;
        }
        public Boolean cariEkle(Int32 cariId, String cariAd, int tur = 1)
        {
            DataTable tbCariM = new DataTable();
            DataLayer.TabloDoldur(ref tbCariM, "SELECT TOP 1 * FROM CARI WHERE CARI_TUR=" + tur.ToString(), EzServer);
            String IKomut = "INSERT INTO CARI (";
            String IKolon = "";
            if (tbCariM != null && tbCariM.Rows.Count > 0)
            {
                foreach (DataColumn Cln in tbCariM.Columns)
                {
                    IKomut += Cln.ColumnName.ToString() + ",";
                    if (Cln.ColumnName.Equals("ID"))
                    {
                        IKolon += cariId.ToString() + ",";
                    }
                    else
                    if (Cln.ColumnName.Equals("AD"))
                    {
                        IKolon += "'" + cariAd.ToString() + "',";
                    }
                    else
                    if (Cln.ColumnName.Equals("KOD"))
                    {
                        IKolon += "'XCari-" + cariId.ToString() + "',";
                    }
                    else
                        IKolon += Cln.ColumnName.ToString() + ",";
                }
                IKomut = IKomut.TrimEnd(',') + ") SELECT " + IKolon.TrimEnd(',') + " FROM CARI WHERE ID=" + tbCariM.Rows[0]["ID"].ToString();
                if (DataLayer.SQLCalistir(IKomut, EzServer))
                {
                    return true;
                }
                else return false;

            }
            return false;
        }
        public Boolean stokEkle(Int32 Id, String ad, String barkod, Int32 stokBirim, DatabaseInfo skDBI)
        {
            DataTable tbStokM = new DataTable();
            DataLayer.TabloDoldur(ref tbStokM, "SELECT TOP 1 * FROM STOK WHERE STOK_CINSI=1", skDBI);
            String IKomut = "INSERT INTO STOK (";
            String IKolon = "";
            if (tbStokM != null && tbStokM.Rows.Count > 0)
            {
                foreach (DataColumn Cln in tbStokM.Columns)
                {
                    IKomut += Cln.ColumnName.ToString() + ",";
                    if (Cln.ColumnName.Equals("ID"))
                    {
                        IKolon += Id.ToString() + ",";
                    }
                    else
                    if (Cln.ColumnName.Equals("AD"))
                    {
                        IKolon += "'" + ad.ToString() + "',";
                    }
                    else
                    if (Cln.ColumnName.Equals("KOD"))
                    {
                        IKolon += "'STKX-" + Id.ToString() + "',";
                    }
                    else
                        IKolon += Cln.ColumnName.ToString() + ",";
                }
                IKomut = IKomut.TrimEnd(',') + ") SELECT " + IKolon.TrimEnd(',') + " FROM STOK WHERE ID=" + tbStokM.Rows[0]["ID"].ToString();
                if (DataLayer.SQLCalistir(IKomut, skDBI))
                {
                    Int32 SSBId = DataLayer.SQLSequences("STOK_STOK_BIRIM", skDBI);
                    String BKomut = "INSERT INTO STOK_STOK_BIRIM (ID,STOK,STOK_BIRIM,CARPAN,VARSAYILAN,AGIRLIK,HACIM,EN,BOY,YUKSEKLIK) VALUES (";
                    BKomut += SSBId.ToString() + "," + Id.ToString() + "," + stokBirim.ToString() + ",1,1,0,0,0,0,0)";
                    if (DataLayer.SQLCalistir(BKomut, skDBI))
                    {
                        Int32 barkodId = DataLayer.SQLInt32("SELECT TOP 1 ID FROM STOK_BARKOD WHERE BARKOD='" + barkod + "'", -1, skDBI);
                        String CKomut = "";
                        if (barkodId == -1)
                        {
                            barkodId = DataLayer.SQLSequences("STOK_BARKOD", skDBI);
                            CKomut = "INSERT INTO STOK_BARKOD (ID,STOK_STOK_BIRIM,BARKOD,KISAYOL,AKTIF) VALUES (";
                            CKomut += barkodId.ToString() + "," + SSBId + ",'" + barkod + "',1,1)";
                        }
                        else
                        {
                            CKomut = "UPDATE STOK_BARKOD SET STOK_STOK_BIRIM=" + SSBId + " WHERE ID=" + barkodId.ToString();
                        }
                        if (DataLayer.SQLCalistir(CKomut, skDBI))
                        {
                            return true;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

    }
}
