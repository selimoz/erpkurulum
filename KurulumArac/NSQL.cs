﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KurulumArac
{
    public partial class NSQL : Form
    {
        public Boolean SQLKurulum=false;
        public String SQLParola = "";
        public String SQLServerName = Environment.MachineName.ToString() + @"\SQL2014";
        

        SqlKur Sq = new SqlKur(Application.StartupPath.ToString());
        public NSQL()
        {
            InitializeComponent();
            txPassword.Text = Sq.ParolaUret();
            SQLParola = txPassword.Text;
        }

        private void btKurBaslat_Click(object sender, EventArgs e)
        {
            if (InstansVar(txInstansName.Text.Trim()))
            {
                MessageBox.Show("Instans Name daha önceden kulllanılmış. Farklı bir Instans name kullanmalısınız.");
                txInstansName.Focus();
                return;
            }
            if (txPassword.Text.Trim() == "")
            {
                MessageBox.Show("SQL Server için sa parolası girilmemiş.");
                txPassword.Focus();
                return;
            }

            tsAciklama.Text = "SQL Server Setup İndiriliyor Bekleyiniz...!";
            Application.DoEvents();
            if (Sq.SqlSetupIndir())
            {
                tsAciklama.Text = "SQL Server Setup Kuruluyor Bekleyiniz...!";
                Application.DoEvents();

                SQLParola = txPassword.Text;
                SQLServerName = Environment.MachineName.ToString() + @"\" + txInstansName.Text.ToUpper().Trim();

                string config = "";
                config += " /QS /ACTION=Install /INSTANCENAME=" + txInstansName.Text.ToUpper().Trim() + " /INSTANCEID=" + txInstansName.Text.ToUpper().Trim() + " /UpdateEnabled=false /ENU=True  /IACCEPTSQLSERVERLICENSETERMS  /SECURITYMODE=SQL /SAPWD=" + txPassword.Text.Trim();
                config += " /TCPENABLED=1 /NPENABLED=1 /BROWSERSVCSTARTUPTYPE=Automatic /ROLE=AllFeatures_WithDefaults /SQMREPORTING=False  /AGTSVCSTARTUPTYPE=Automatic /ISSVCSTARTUPTYPE=Automatic";
                config += " /FEATURES=SQLENGINE,REPLICATION,FULLTEXT,DQ,AS,RS,RS_SHP,RS_SHPWFE,DQC,CONN,IS,BC,SDK,BOL,SSMS,ADV_SSMS,DREPLAY_CTLR,DREPLAY_CLT,SNAC_SDK,MDS";
                config += " /ASSVCSTARTUPTYPE=Automatic /ASCOLLATION=Turkish_CI_AS /HELP=False  /X86=False  /ERRORREPORTING=False";
                config += " /ASDATADIR=Data /ASLOGDIR=Log /ASBACKUPDIR=Backup /ASTEMPDIR=Temp /ASCONFIGDIR=Config /ASPROVIDERMSOLAP=1 /SQLSVCSTARTUPTYPE=Automatic /FILESTREAMLEVEL=0";
                config += " /ENABLERANU=True /SQLCOLLATION=Turkish_CI_AS /RSSVCSTARTUPTYPE=Automatic /RSINSTALLMODE=FilesOnlyMode";

                Process pro = new Process();
                pro.StartInfo.FileName = Application.StartupPath.ToString() + @"\data\sql\SQL2014Kur.exe";
                pro.StartInfo.Arguments = config;
                pro.StartInfo.CreateNoWindow = true;
                pro.Start();
                pro.WaitForExit();

                SQLKurulum = true;

                MessageBox.Show("SQL Server Kurulumu Tamamlandı..!");
                this.Close();
            }

        }
        private Boolean InstansVar(String IName)
        {
            Boolean Sonuc = false;
            DatabaseInfo DL = new DatabaseInfo();
            foreach (String InsName in DL.LocalServer())
            {
                if (InsName.ToUpper() == Environment.MachineName.ToUpper() + @"\" + IName.ToUpper().Trim())
                {
                    return true;
                }
            }
            return Sonuc;
        }

        private void label4_DoubleClick(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            if (txPassword.UseSystemPasswordChar) txPassword.UseSystemPasswordChar = true; else txPassword.UseSystemPasswordChar = false;
        }

        private void tsAciklama_Click(object sender, EventArgs e)
        {
            txPassword.Text = Sq.ParolaUret();
        }

        private void cmYeniParola_Click(object sender, EventArgs e)
        {
            SQLParola = Sq.ParolaUret();
            txPassword.Text = SQLParola;
        }

        private void cmParolaGozukmesin_Click(object sender, EventArgs e)
        {
            
        }

        private void cmParolaGozukmesin_CheckedChanged(object sender, EventArgs e)
        {
            txPassword.UseSystemPasswordChar = cmParolaGozukmesin.Checked;
        }
    }
}
