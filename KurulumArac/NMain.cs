﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace KurulumArac
{
    public partial class NMain : Form
    {

        private Boolean ConOpen;
        Boolean Kaydedildi = true;
        DatabaseInfo secData;
        Araclar ARC = new Araclar();
        ErpAyarlar EA = new ErpAyarlar();
        public NMain()
        {
            InitializeComponent();

        }
        private void NMain_Load(object sender, EventArgs e)
        {
            LocalHostYukle();
            if (cmbServer.Items.Count > 0) cmbServer.SelectedIndex = 0;
            txtUserName.Text = "sa";
            ConOpen = false;
            secData = new DatabaseInfo();
            EA.DataBaseYukle(ref listSunucu);
            lbPcPosLocal.Text = "Sunucu : " + EA.DBPcPosLocal.Host + ", Veritabanı : " + EA.DBPcPosLocal.DataCatalog;
            lbPCPosServer.Text = "Sunucu : " + EA.DBPcPosServer.Host + ", Veritabanı : " + EA.DBPcPosServer.DataCatalog;
            lbEZLocal.Text = "Sunucu : " + EA.DBPcPosLocal.Host + ", Veritabanı : " + EA.DBPcPosLocal.DataCatalog;
            lbEZServer.Text = "Sunucu : " + EA.DBPcPosServer.Host + ", Veritabanı : " + EA.DBPcPosServer.DataCatalog;
        }
        private void LocalHostYukle()
        {
            DatabaseInfo DL = new DatabaseInfo();
            List<String> L = DL.LocalServer();
            if (L.Count > 0)
            {
                foreach (String item in L)
                {
                    if (!cmbServer.Items.Contains(item))
                    {
                        cmbServer.Items.Add(item);
                    }
                }

            }

        }
        private void chkPassLabel_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPassLabel.Checked) txtPassword.UseSystemPasswordChar = false; else txtPassword.UseSystemPasswordChar = true;
        }
        private void btnDBTest_Click(object sender, EventArgs e)
        {
            BaglantiDene();
        }
        private void BaglantiDene()
        {
            listCatalog.Items.Clear();

            if (cmbServer.Text.Trim() == "")
            {
                MessageBox.Show("Ana Makina Girilmemiş..!");
                cmbServer.Focus();
                return;
            }
            else secData.Host = cmbServer.Text;
            if (txtUserName.Text.Trim() == "")
            {
                MessageBox.Show("Kullanıcı Bilgisi Girilmedi..!");
                txtUserName.Focus();
                return;
            }
            else secData.Userid = txtUserName.Text;
            if (txtPassword.Text.Trim() == "")
            {
                MessageBox.Show("Parola Girilmedi..!");
                txtPassword.Focus();
                return;
            }
            else secData.Password = txtPassword.Text;
            secData.DataCatalog = "master";
            tsVtAciklama.Text = "Kontrol Ediliyor..!";
            tsVtAciklama.ForeColor = Color.Black;
            Application.DoEvents();
            btnSunucuEkle.Enabled = false;
            btnSunucuOlustur.Enabled = false;
            secData.ConnectionStringCreate();
            if (DataLayer.BaglantiTest(secData))
            {
                ConOpen = true;
                tsVtAciklama.Text = "Bağlantı var";
                DataLayer.masterDB = secData;
                tsVtAciklama.ForeColor = Color.DarkGreen;
                DataTable tbCat = new DataTable();
                DataLayer.TabloDoldur(ref tbCat, "SELECT * FROM sys.databases where LOWER(name) not in ('master','tempdb','model','msdb')", secData);
                if (tbCat != null && tbCat.Rows.Count > 0)
                {
                    foreach (DataRow RCat in tbCat.Rows)
                    {
                        listCatalog.Items.Add(RCat["name"].ToString());
                    }
                    btnSunucuEkle.Enabled = true;

                }
            }
            else
            {
                tsVtAciklama.Text = "Bağlantı Sağlanamadı (" + DataLayer.HataMesaj.Message + ")";
                tsVtAciklama.Tag = DataLayer.HataMesaj.ToString();
                tsVtAciklama.ForeColor = Color.DarkRed;
            }
            String[] HostPc = secData.Host.Split('\\');
            if (HostPc.Length > 0 && HostPc[0].ToString().ToUpper().Trim() == Environment.MachineName.ToString().ToUpper().Trim()) btnSunucuOlustur.Enabled = true;
        }
        private void cmbServer_TextChanged(object sender, EventArgs e)
        {
            BaglantiKapandi();
        }
        private void BaglantiKapandi()
        {
            ConOpen = false;
            listCatalog.Items.Clear();
            btnSunucuEkle.Enabled = false;
            btnSunucuOlustur.Enabled = false;
            tsVtAciklama.Text = "";
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            BaglantiKapandi();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            BaglantiKapandi();
        }

        private void tsVtAciklama_Click(object sender, EventArgs e)
        {
            if (tsVtAciklama.Tag != null)
            {
                MessageBox.Show(tsVtAciklama.Tag.ToString());
            }
        }

        private void btnSunucuOlustur_Click(object sender, EventArgs e)
        {
            YeniDataOlustur();
            if (DataLayer.YeniDataAdi != "")
            {
                SunucuEkle(DataLayer.YeniDataAdi);
            }
        }
        private void YeniDataOlustur()
        {
            if (ConOpen)
            {
                DataLayer.YeniDataAdi = "";
                DataLayer.AktifDB.Host = secData.Host;
                DataLayer.AktifDB.Userid = secData.Userid;
                DataLayer.AktifDB.Password = secData.Password;
                NYeniData YD = new NYeniData();
                YD.ShowDialog();
                YD.Dispose();
                BaglantiDene();
                if (DataLayer.YeniDataAdi != "")
                {
                    MessageBox.Show(DataLayer.YeniDataAdi + " Veritabanı Eklendi.", "BİLGİ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            YeniDataOlustur();
        }

        private void btnSunucuEkle_Click(object sender, EventArgs e)
        {
            SunucuEkle();
        }
        private void SunucuEkle(String SunucuAd = "")
        {

            if (SunucuAd == "" && listCatalog.Items.Count > 0 && listCatalog.SelectedIndex > -1)
            {
                SunucuAd = listCatalog.Text;
            }
            Boolean tekrar = false;
            if (EA.DBList.Count > 0)
            {
                foreach (DatabaseInfo DI in EA.DBList)
                {
                    if (DI.Host != null && DI.DataCatalog != null && DI.Host.Trim().ToUpper() == cmbServer.Text.Trim().ToUpper() & DI.DataCatalog.Trim().ToUpper() == SunucuAd.Trim().ToString())
                    {
                        DialogResult Cevap = MessageBox.Show("Eklemek İstediğiniz Veritabanı Listede Zaten Var. Tekrar Eklensinmi?", "Tekrar Onayı", MessageBoxButtons.YesNo);
                        if (Cevap.ToString() != "Yes")
                        {
                            tekrar = true;
                            return;
                        }
                    }
                }
            }
            Kaydedildi = false;
            DatabaseInfo DB = new DatabaseInfo();
            DB.Host = cmbServer.Text;
            DB.Userid = txtUserName.Text;
            DB.Password = txtPassword.Text;
            DB.DataCatalog = SunucuAd.Trim();
            DB.ConnectionStringCreate();
            DB.Text = DB.Host + ">" + DB.DataCatalog;
            if (tekrar) DB.Text += "1";
            EA.DBList.Add(DB);
            listSunucu.Items.Add(DB.Text + " [ Sunucu : " + DB.Host + "], [Veritabanı : " + DB.DataCatalog + " ]");

        }

        private void btnSunucuSil_Click(object sender, EventArgs e)
        {
            if (listSunucu.Items.Count > 0 && listSunucu.SelectedIndex > -1)
            {
                DialogResult Cevap = MessageBox.Show("Seçili ERP Bağlantısı Silinsinmi?", "Silme Onayı", MessageBoxButtons.YesNo);
                if (Cevap.ToString() == "Yes")
                {
                    if (EA.DBList.Count > 0)
                    {
                        foreach (DatabaseInfo DI in EA.DBList)
                        {
                            String Deger = DI.Text + " [ Sunucu : " + DI.Host + "], [Veritabanı : " + DI.DataCatalog + " ]";
                            if (Deger == listSunucu.Text)
                            {
                                EA.DBList.Remove(DI);
                                listSunucu.Items.RemoveAt(listSunucu.SelectedIndex);
                                Kaydedildi = false;
                                return;
                            }
                        }
                    }

                }
            }
        }

        private void btnAyarKaydet_Click(object sender, EventArgs e)
        {
            EA.Kaydet();
            Kaydedildi = true;
        }

        private void menuPCPosLocalSet_Click(object sender, EventArgs e)
        {
            if (EA.DBList.Count > 0)
            {
                foreach (DatabaseInfo DI in EA.DBList)
                {
                    String Deger = DI.Text + " [ Sunucu : " + DI.Host + "], [Veritabanı : " + DI.DataCatalog + " ]";
                    if (Deger == listSunucu.Text)
                    {
                        if (EA.DBPcPosLocal == null) EA.DBPcPosLocal = new DatabaseInfo();
                        EA.DBPcPosLocal = DI;
                        lbPcPosLocal.Text = "Sunucu : " + DI.Host + ", Veritabanı : " + DI.DataCatalog;
                        lbEZLocal.Text = "Sunucu : " + DI.Host + ", Veritabanı : " + DI.DataCatalog;
                        EA.PCPosDbDegisti = true;
                        Kaydedildi = false;
                    }
                }
            }
        }

        private void menuPcPosServerSet_Click(object sender, EventArgs e)
        {
            if (EA.DBList.Count > 0)
            {
                foreach (DatabaseInfo DI in EA.DBList)
                {
                    String Deger = DI.Text + " [ Sunucu : " + DI.Host + "], [Veritabanı : " + DI.DataCatalog + " ]";
                    if (Deger == listSunucu.Text)
                    {
                        if (EA.DBPcPosServer == null) EA.DBPcPosServer = new DatabaseInfo();
                        EA.DBPcPosServer = DI;
                        lbPCPosServer.Text = "Sunucu : " + DI.Host + ", Veritabanı : " + DI.DataCatalog;
                        lbEZServer.Text = "Sunucu : " + DI.Host + ", Veritabanı : " + DI.DataCatalog;
                        EA.PCPosDbDegisti = true;
                        Kaydedildi = false;
                    }
                }
            }
        }
        private Boolean InternetVarmi()
        {
            Ping Pn = new Ping();
            PingReply PingDurum = Pn.Send(IPAddress.Parse("23.209.100.228"));
            if (PingDurum.Status == IPStatus.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private Boolean BilgisayarAdKontrol()
        {
            Boolean Sonuc = true;


            String PCAd = Environment.MachineName.ToString().ToUpper(new CultureInfo("en-US", false));
            if (PCAd.Contains("İ")) Sonuc = false;
            if (PCAd.Contains("Ş")) Sonuc = false;
            if (PCAd.Contains("Ğ")) Sonuc = false;
            if (PCAd.Contains("Ü")) Sonuc = false;
            if (PCAd.Contains("Ç")) Sonuc = false;
            if (PCAd.Contains("Ö")) Sonuc = false;
            if (PCAd.Contains(" ")) Sonuc = false;
            if (!Sonuc)
            {
                if (MessageBox.Show("Bilgisayar Adı Problemli ...!\r\n \r\nBilgisayar adında (işüğçö vs..) karakterler olamaz. Bilgisayar adını değiştirmek istermisiniz?", "Uyarı", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    tsAciklama.BackColor = Color.Red;
                    tsAciklama.ForeColor = Color.White;
                    tsAciklama.Text = "Bilgisayarınız Yeniden Başlatılacak Bekleyiniz...!";

                    Application.DoEvents();
                    BilgisayarAdDegistir();
                    //Restart();
                }
            }
            return Sonuc;
        }
        private static void BilgisayarAdDegistir()
        {
            Process Prc = new Process();
            Prc.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            Prc.StartInfo.FileName = "PNC.BAT";
            Prc.StartInfo.Arguments = " " + Environment.MachineName.ToString() + " ERPSERVER" + DateTime.Now.ToString("yyMMdd");
            Prc.Start();
        }
        private static void Restart()
        {
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.WindowStyle = ProcessWindowStyle.Hidden;
            proc.FileName = "shutdown.exe";
            proc.Arguments = " /r /t 0";
            Process.Start(proc);
        }
        private void btnSQLKur_Click(object sender, EventArgs e)
        {
            if (!BilgisayarAdKontrol()) return;
            if (InternetVarmi() | File.Exists(Application.StartupPath + @"\data\sql\SQL2014Kur.exe"))
            {
                NSQL ff = new NSQL();
                ff.ShowDialog();
                if (ff.SQLKurulum)
                {
                    txtPassword.Text = ff.SQLParola;
                    if (cmbServer.Items.Contains(ff.SQLServerName))
                    {
                        cmbServer.SelectedValue = ff.SQLServerName;
                    }
                    else
                    {
                        cmbServer.Text = ff.SQLServerName;
                    }
                    MessageBox.Show("SQL Server Kuruldu ...! \r\n \r\nBağlantı [Sına] yaptıktan sonra [Yeni ERP Veritabanı Oluştur] 'a tıklayıp veritabanınızı oluşturunuz.");
                }
                ff.Dispose();
                LocalHostYukle();
            }
            else MessageBox.Show("Internet bağlantınız ya da https://download.microsoft.com sitesine bağlantı yok. Kontrol ediniz.");
        }

        private void rbIstemci_CheckedChanged(object sender, EventArgs e)
        {
            btnSQLKur.Enabled = rbSunucu.Checked;
        }


        private void NMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Kaydedildi)
            {
                DialogResult DR = MessageBox.Show("Yaptığınız Ayarlar Kaydedilmedi. Kaydetmek İstermisiniz?", "UYARI", MessageBoxButtons.YesNoCancel);
                if (DR == DialogResult.Cancel)
                {
                    e.Cancel = false;
                }
                if (DR == DialogResult.Yes)
                {
                    EA.Kaydet();
                }


            }
        }

        private void btnGuncelleme_Click(object sender, EventArgs e)
        {
            GuncellemeAc();
        }

        private void GuncellemeAc()
        {
            if (!Kaydedildi)
            {
                DialogResult DR = MessageBox.Show("Ayarlar Kaydedilsinmi?", "Kaydetme Onayı", MessageBoxButtons.YesNo);
                if (DR == DialogResult.Yes) EA.Kaydet();
            }
            if (!File.Exists(Application.StartupPath + @"\Guncelleme.exe"))
            {
                OpenFileDialog FD = new OpenFileDialog();
                FD.Title = "Güncelleme.Exe Yerini Gösteriniz";
                FD.Filter = "Güncelleme|Guncelleme.exe";
                FD.InitialDirectory = Application.StartupPath;
                if (FD.ShowDialog() == DialogResult.OK)
                {
                    String DosyaAd = FD.FileName.ToString();
                    if (DosyaAd != "")
                    {
                        Process Pro = new Process();
                        Pro.StartInfo.FileName = DosyaAd;
                        Pro.Start();
                        Pro.WaitForExit();
                    }
                    else
                    {
                        MessageBox.Show("Guncelleme.exe bulunamadı..!");
                        return;
                    }
                }
            }
            else
            {
                Process Pro = new Process();
                Pro.StartInfo.FileName = Application.StartupPath + @"\Guncelleme.exe";
                Pro.Start();
                Pro.WaitForExit();
            }
        }
        private void ServisAc()
        {
            if (!Kaydedildi)
            {
                DialogResult DR = MessageBox.Show("Ayarlar Kaydedilsinmi?", "Kaydetme Onayı", MessageBoxButtons.YesNo);
                if (DR == DialogResult.Yes) EA.Kaydet();
            }

            if (!File.Exists(Application.StartupPath + @"\Servis.exe"))
            {
                OpenFileDialog FD = new OpenFileDialog();
                FD.Title = "Servis.Exe Yerini Gösteriniz";
                FD.Filter = "Servis|Servis.exe";
                FD.InitialDirectory = Application.StartupPath;
                if (FD.ShowDialog() == DialogResult.OK)
                {
                    String DosyaAd = FD.FileName.ToString();
                    if (DosyaAd != "")
                    {
                        Process Pro = new Process();
                        Pro.StartInfo.FileName = DosyaAd;
                        Pro.Start();
                        Pro.WaitForExit();
                    }
                    else
                    {
                        MessageBox.Show("Servis.exe bulunamadı..!");
                        return;
                    }
                }
            }
            else
            {
                Process Pro = new Process();
                Pro.StartInfo.FileName = Application.StartupPath + @"\Servis.exe";
                Pro.Start();
                Pro.WaitForExit();
            }
        }

        private void btnServis_Click(object sender, EventArgs e)
        {
            ServisAc();
        }

        private void cmBilgisayarAdDegis_Click(object sender, EventArgs e)
        {
            BilgisayarAdDegistir();
        }

        private void cmYenidenBaslat_Click(object sender, EventArgs e)
        {
            Restart();
        }

        private void btnFiyatEsitle_Click(object sender, EventArgs e)
        {
            EsZamanlama EZ = new EsZamanlama(EA.DBPcPosLocal.ConnectionString, EA.DBPcPosServer.ConnectionString, listEsZaman);
            EZ.TabloGuncelle("STOK_STOK_BIRIM_FIYAT", ref PBar);

        }

        private void menuRaporKullanicisi_Click(object sender, EventArgs e)
        {
            if (!ConOpen)
            {
                BaglantiDene();
            }
            else
            {
                DataLayer.RaporUserTemizle();
                DataLayer.RaporUserEkle();
            }

        }

        private void menuSaParolaDegistir_Click(object sender, EventArgs e)
        {
            String YParola = DataLayer.SQLUserPasswordChange(cmbServer.Text, txtUserName.Text);
            if (YParola != "")
            {
                txtPassword.Text = YParola;
                chkPassLabel.Checked = true;
            }
        }

        private void btnStokEsitle_Click(object sender, EventArgs e)
        {
            EsZamanlama EZ = new EsZamanlama(EA.DBPcPosLocal.ConnectionString, EA.DBPcPosServer.ConnectionString, listEsZaman);
            EZ.TabloGuncelle("STOK_FIYAT_AD", ref PBar);
            EZ.TabloGuncelle("STOK_BIRIM", ref PBar);
            EZ.TabloGuncelle("STOK_GRUP", ref PBar);
            EZ.TabloGuncelle("STOK_MARKA", ref PBar);
            EZ.TabloGuncelle("STOK", ref PBar);
            EZ.TabloGuncelle("STOK_STOK_BIRIM", ref PBar);
            EZ.TabloGuncelle("STOK_STOK_BIRIM_FIYAT", ref PBar);
            EZ.TabloGuncelle("STOK_BARKOD", ref PBar);

        }

        private void btnListele_Click(object sender, EventArgs e)
        {
            EsZamanlama EZ = new EsZamanlama(EA.DBPcPosLocal.ConnectionString, EA.DBPcPosServer.ConnectionString);
            EZ.problemliEvrakListe(ref gridEvraklar);
            if (gridEvraklar.Rows.Count > 0) btnDuzelt.Enabled = true; else btnDuzelt.Enabled = false;

        }

        private void btnDuzelt_Click(object sender, EventArgs e)
        {
            if (gridEvraklar.Rows.Count > 0)
            {
                EsZamanlama EZ = new EsZamanlama(EA.DBPcPosLocal.ConnectionString, EA.DBPcPosServer.ConnectionString);
                DataTable TB = (DataTable)gridEvraklar.DataSource;
                EZ.problemliEvrakDuzelt(ref TB, ref TXDurum);
            }
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            if (EA.DBServis == null)
            {
                MessageBox.Show("İşlem Yapılacak Veritabanını Seçmelisiniz.");
                return;
            }
            splitContainer1.Panel2.Controls.Clear();
            if (treeView1.SelectedNode != null)
            {
                TreeNode nd = treeView1.SelectedNode;
                if (nd.Text == "Rapor Export")
                {
                    DIGER.EkIslemler EkIs = new DIGER.EkIslemler(EA.DBServis);
                    EkIs.raporExportPanel(ref splitContainer1);
                }
                if (nd.Text == "Rapor Import")
                {
                    using (OpenFileDialog fd = new OpenFileDialog())
                    {
                        fd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        fd.Title = "ERP12 Rapor Import";
                        fd.Filter = "ERP12 Rapor Dosyası|*.erpRapor"; ;
                        if (fd.ShowDialog() == DialogResult.OK)
                        {
                            DIGER.RaporTransfer TRN = new DIGER.RaporTransfer();
                            TRN.RaporImport(fd.FileName, EA.DBServis.ConnectionString);
                            
                        }
                    }
                }
                if (nd.Text =="Fiyat Kopyala")
                {
                    DIGER.EkIslemler FYT = new DIGER.EkIslemler(EA.DBServis);
                    FYT.fiyatDegisimExportPanel(ref splitContainer1);

                }
                if (nd.Text== "Sorgu Çalıştır")
                {
                    DIGER.EkIslemler EkIs = new DIGER.EkIslemler(EA.DBServis);
                    EkIs.SorguPanel(ref splitContainer1);
                }
            }
        }

        private void menuServisAktif_Click(object sender, EventArgs e)
        {
            if (EA.DBList.Count > 0)
            {
                foreach (DatabaseInfo DI in EA.DBList)
                {
                    String Deger = DI.Text + " [ Sunucu : " + DI.Host + "], [Veritabanı : " + DI.DataCatalog + " ]";
                    if (Deger == listSunucu.Text)
                    {
                        if (EA.DBPcPosLocal == null) EA.DBPcPosLocal = new DatabaseInfo();
                        EA.DBServis = DI;
                        lbServisDBI.Text = "Sunucu : " + DI.Host + ", Veritabanı : " + DI.DataCatalog;
                    }
                }
            }

        }
    }
}
